var json_T47IMTAbiomassproductionmonitoring = {
    "type": "FeatureCollection",
    "name": "T47IMTAbiomassproductionmonitoring",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 1,
                "Lat": 40.34957408,
                "Lon": 18.16731277,
                "Parter": "1 Coordinator",
                "Country": "Italy",
                "PI": "Sergio Rossi",
                "Short": "UNISAL",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    18.16731277,
                    40.34957408
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "ID": 6,
                "Lat": 41.39471975,
                "Lon": 2.153917085,
                "Parter": "6 Partner",
                "Country": "Spain",
                "PI": "Marc Garcia-Duran Huet",
                "Short": "UGI",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.153917085,
                    41.39471975
                ]
            }
        },
        {
            "id": "2",
            "type": "Feature",
            "properties": {
                "ID": 7,
                "Lat": 28.09909899,
                "Lon": -15.41989115,
                "Parter": "7 Partner",
                "Country": "Spain",
                "PI": "Otero Ferrer Francisco",
                "Short": "ULPGC",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -15.41989115,
                    28.09909899
                ]
            }
        },
        {
            "id": "3",
            "type": "Feature",
            "properties": {
                "ID": 11,
                "Lat": 27.99223955,
                "Lon": -15.3686539,
                "Parter": "11 Partner",
                "Country": "Spain",
                "PI": "Eric Delory",
                "Short": "PLOCAN",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -15.3686539,
                    27.99223955
                ]
            }
        }
    ]
};