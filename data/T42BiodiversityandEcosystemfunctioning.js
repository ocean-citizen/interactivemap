var json_T42BiodiversityandEcosystemfunctioning = {
    "type": "FeatureCollection",
    "name": "T42BiodiversityandEcosystemfunctioning",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 1,
                "Lat": 40.34957408,
                "Lon": 18.16731277,
                "Parter": "1 Coordinator",
                "Country": "Italy",
                "PI": "Sergio Rossi",
                "Short": "UNISAL",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    18.16731277,
                    40.34957408
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "Lat": 48.84775399,
                "Lon": 2.264089621,
                "Parter": "3 Partner",
                "Country": "France",
                "PI": "Lorenzo Bramanti",
                "Short": "CNRS",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.264089621,
                    48.84775399
                ]
            }
        },
        {
            "id": "2",
            "type": "Feature",
            "properties": {
                "ID": 4,
                "Lat": 41.9120566,
                "Lon": 12.47535532,
                "Parter": "4 Partner",
                "Country": "Italy",
                "PI": "Paolo Vassallo",
                "Short": "CONISMA",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.47535532,
                    41.9120566
                ]
            }
        },
        {
            "id": "3",
            "type": "Feature",
            "properties": {
                "ID": 5,
                "Lat": 41.38530625,
                "Lon": 2.196064293,
                "Parter": "5 Partner",
                "Country": "Spain",
                "PI": "Enrique Isla",
                "Short": "CSIC",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.196064293,
                    41.38530625
                ]
            }
        },
        {
            "id": "4",
            "type": "Feature",
            "properties": {
                "ID": 8,
                "Lat": 41.38680116,
                "Lon": 2.16393047,
                "Parter": "8 Partner",
                "Country": "Spain",
                "PI": "Andrea Gori",
                "Short": "UB",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.16393047,
                    41.38680116
                ]
            }
        },
        {
            "id": "5",
            "type": "Feature",
            "properties": {
                "ID": 10,
                "Lat": 53.27930741,
                "Lon": -9.061184376,
                "Parter": "10 Partner",
                "Country": "Irland",
                "PI": "Louise Allcock",
                "Short": "NUIG",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -9.061184376,
                    53.27930741
                ]
            }
        },
        {
            "id": "6",
            "type": "Feature",
            "properties": {
                "ID": 14,
                "Lat": 55.78602365,
                "Lon": 12.52439728,
                "Parter": "14 Partner",
                "Country": "Danmark",
                "PI": "Patrizio Mariani",
                "Short": "DTU",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.52439728,
                    55.78602365
                ]
            }
        },
        {
            "id": "7",
            "type": "Feature",
            "properties": {
                "ID": 18,
                "Lat": 53.53331276,
                "Lon": 8.58049404,
                "Parter": "18 Partner",
                "Country": "Germany",
                "PI": "Charlotte Havermans",
                "Short": "AWI",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    8.58049404,
                    53.53331276
                ]
            }
        },
        {
            "id": "8",
            "type": "Feature",
            "properties": {
                "ID": 20,
                "Lat": 60.3999584,
                "Lon": 5.303981996,
                "Parter": "20 Partner",
                "Country": "Norway",
                "PI": "Hans Kristian Strand",
                "Short": "IMR",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    5.303981996,
                    60.3999584
                ]
            }
        },
        {
            "id": "9",
            "type": "Feature",
            "properties": {
                "ID": 22,
                "Lat": 28.18145479,
                "Lon": -16.79435009,
                "Parter": "23 Partner",
                "Country": "Spain",
                "PI": "Carlos Mallo",
                "Short": "Innoceana",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.79435009,
                    28.18145479
                ]
            }
        },
        {
            "id": "10",
            "type": "Feature",
            "properties": {
                "ID": 23,
                "Lat": 41.38642486,
                "Lon": 2.177551941,
                "Parter": "23 Partner",
                "Country": "Spain",
                "PI": "Carlos Mallo",
                "Short": "Innoceana",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.177551941,
                    41.38642486
                ]
            }
        }
    ]
};