var json_T12Historicalbaseline = {
    "type": "FeatureCollection",
    "name": "T12Historicalbaseline",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 1,
                "Lat": 40.34957408,
                "Lon": 18.16731277,
                "Parter": "1 Coordinator",
                "Country": "Italy",
                "PI": "Sergio Rossi",
                "Short": "UNISAL",
                "Style_URL": "./Style/T1.qml",
                "svg_URL": "./Style/T1.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    18.16731277,
                    40.34957408
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "ID": 12,
                "Lat": 50.7369236,
                "Lon": -3.534859647,
                "Parter": "12 Partner",
                "Country": "United Kingdom",
                "PI": "Ruth Thurstan",
                "Short": "UNEXE",
                "Style_URL": "./Style/T1.qml",
                "svg_URL": "./Style/T1.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -3.534859647,
                    50.7369236
                ]
            }
        }
    ]
};