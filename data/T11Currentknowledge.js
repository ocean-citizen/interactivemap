var json_T11Currentknowledge = {
    "type": "FeatureCollection",
    "name": "T11Currentknowledge",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 1,
                "Lat": 40.34957408,
                "Lon": 18.16731277,
                "Parter": "1 Coordinator",
                "Country": "Italy",
                "PI": "Sergio Rossi",
                "Short": "UNISAL",
                "Style_URL": "./Style/T1.qml",
                "svg_URL": "./Style/T1.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    18.16731277,
                    40.34957408
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "ID": 8,
                "Lat": 41.38680116,
                "Lon": 2.16393047,
                "Parter": "8 Partner",
                "Country": "Spain",
                "PI": "Andrea Gori",
                "Short": "UB",
                "Style_URL": "./Style/T1.qml",
                "svg_URL": "./Style/T1.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.16393047,
                    41.38680116
                ]
            }
        },
        {
            "id": "2",
            "type": "Feature",
            "properties": {
                "ID": 12,
                "Lat": 50.7369236,
                "Lon": -3.534859647,
                "Parter": "12 Partner",
                "Country": "United Kingdom",
                "PI": "Ruth Thurstan",
                "Short": "UNEXE",
                "Style_URL": "./Style/T1.qml",
                "svg_URL": "./Style/T1.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -3.534859647,
                    50.7369236
                ]
            }
        },
        {
            "id": "3",
            "type": "Feature",
            "properties": {
                "ID": 15,
                "Lat": 29.55442633,
                "Lon": 34.97036524,
                "Parter": "15 Partner",
                "Country": "Israel",
                "PI": "Julia Rozental",
                "Short": "V-CORALS",
                "Style_URL": "./Style/T1.qml",
                "svg_URL": "./Style/T1.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    34.97036524,
                    29.55442633
                ]
            }
        },
        {
            "id": "4",
            "type": "Feature",
            "properties": {
                "ID": 20,
                "Lat": 60.3999584,
                "Lon": 5.303981996,
                "Parter": "20 Partner",
                "Country": "Norway",
                "PI": "Hans Kristian Strand",
                "Short": "IMR",
                "Style_URL": "./Style/T1.qml",
                "svg_URL": "./Style/T1.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    5.303981996,
                    60.3999584
                ]
            }
        }
    ]
};