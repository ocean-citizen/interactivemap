var json_T73Networking = {
    "type": "FeatureCollection",
    "name": "T73Networking",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 1,
                "Lat": 40.34957408,
                "Lon": 18.16731277,
                "Parter": "1 Coordinator",
                "Country": "Italy",
                "PI": "Sergio Rossi",
                "Short": "UNISAL",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    18.16731277,
                    40.34957408
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "ID": 2,
                "Lat": 32.82616636,
                "Lon": 34.95663232,
                "Parter": "2 Partner",
                "Country": "Israel",
                "PI": "Baruch Rinkevich",
                "Short": "IOLR",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    34.95663232,
                    32.82616636
                ]
            }
        },
        {
            "id": "2",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "Lat": 48.84775399,
                "Lon": 2.264089621,
                "Parter": "3 Partner",
                "Country": "France",
                "PI": "Lorenzo Bramanti",
                "Short": "CNRS",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.264089621,
                    48.84775399
                ]
            }
        },
        {
            "id": "3",
            "type": "Feature",
            "properties": {
                "ID": 4,
                "Lat": 41.9120566,
                "Lon": 12.47535532,
                "Parter": "4 Partner",
                "Country": "Italy",
                "PI": "Paolo Vassallo",
                "Short": "CONISMA",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.47535532,
                    41.9120566
                ]
            }
        },
        {
            "id": "4",
            "type": "Feature",
            "properties": {
                "ID": 5,
                "Lat": 41.38530625,
                "Lon": 2.196064293,
                "Parter": "5 Partner",
                "Country": "Spain",
                "PI": "Enrique Isla",
                "Short": "CSIC",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.196064293,
                    41.38530625
                ]
            }
        },
        {
            "id": "5",
            "type": "Feature",
            "properties": {
                "ID": 6,
                "Lat": 41.39471975,
                "Lon": 2.153917085,
                "Parter": "6 Partner",
                "Country": "Spain",
                "PI": "Marc Garcia-Duran Huet",
                "Short": "UGI",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.153917085,
                    41.39471975
                ]
            }
        },
        {
            "id": "6",
            "type": "Feature",
            "properties": {
                "ID": 7,
                "Lat": 28.09909899,
                "Lon": -15.41989115,
                "Parter": "7 Partner",
                "Country": "Spain",
                "PI": "Otero Ferrer Francisco",
                "Short": "ULPGC",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -15.41989115,
                    28.09909899
                ]
            }
        },
        {
            "id": "7",
            "type": "Feature",
            "properties": {
                "ID": 8,
                "Lat": 41.38680116,
                "Lon": 2.16393047,
                "Parter": "8 Partner",
                "Country": "Spain",
                "PI": "Andrea Gori",
                "Short": "UB",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.16393047,
                    41.38680116
                ]
            }
        },
        {
            "id": "8",
            "type": "Feature",
            "properties": {
                "ID": 9,
                "Lat": 44.41150874,
                "Lon": 8.929847878,
                "Parter": "9 Partner",
                "Country": "Italy",
                "PI": "Marco Palma",
                "Short": "UBICA",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    8.929847878,
                    44.41150874
                ]
            }
        },
        {
            "id": "9",
            "type": "Feature",
            "properties": {
                "ID": 10,
                "Lat": 53.27930741,
                "Lon": -9.061184376,
                "Parter": "10 Partner",
                "Country": "Irland",
                "PI": "Louise Allcock",
                "Short": "NUIG",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -9.061184376,
                    53.27930741
                ]
            }
        },
        {
            "id": "10",
            "type": "Feature",
            "properties": {
                "ID": 11,
                "Lat": 27.99223955,
                "Lon": -15.3686539,
                "Parter": "11 Partner",
                "Country": "Spain",
                "PI": "Eric Delory",
                "Short": "PLOCAN",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -15.3686539,
                    27.99223955
                ]
            }
        },
        {
            "id": "11",
            "type": "Feature",
            "properties": {
                "ID": 12,
                "Lat": 50.7369236,
                "Lon": -3.534859647,
                "Parter": "12 Partner",
                "Country": "United Kingdom",
                "PI": "Ruth Thurstan",
                "Short": "UNEXE",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -3.534859647,
                    50.7369236
                ]
            }
        },
        {
            "id": "12",
            "type": "Feature",
            "properties": {
                "ID": 13,
                "Lat": 41.3885732,
                "Lon": 2.175964782,
                "Parter": "13 Partner",
                "Country": "Spain",
                "PI": "Juanita Zorrilla Pujana",
                "Short": "SUBMON",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.175964782,
                    41.3885732
                ]
            }
        },
        {
            "id": "13",
            "type": "Feature",
            "properties": {
                "ID": 14,
                "Lat": 55.78602365,
                "Lon": 12.52439728,
                "Parter": "14 Partner",
                "Country": "Danmark",
                "PI": "Patrizio Mariani",
                "Short": "DTU",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.52439728,
                    55.78602365
                ]
            }
        },
        {
            "id": "14",
            "type": "Feature",
            "properties": {
                "ID": 15,
                "Lat": 29.55442633,
                "Lon": 34.97036524,
                "Parter": "15 Partner",
                "Country": "Israel",
                "PI": "Julia Rozental",
                "Short": "V-CORALS",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    34.97036524,
                    29.55442633
                ]
            }
        },
        {
            "id": "15",
            "type": "Feature",
            "properties": {
                "ID": 16,
                "Lat": 54.17939719,
                "Lon": 12.08118254,
                "Parter": "16 Partner",
                "Country": "Germany",
                "PI": "Peter Feldens",
                "Short": "IOW",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.08118254,
                    54.17939719
                ]
            }
        },
        {
            "id": "16",
            "type": "Feature",
            "properties": {
                "ID": 17,
                "Lat": 54.33989425,
                "Lon": 10.12008974,
                "Parter": "17 Partner",
                "Country": "Germany",
                "PI": "Jens Schneider von Deimling",
                "Short": "CAU",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    10.12008974,
                    54.33989425
                ]
            }
        },
        {
            "id": "17",
            "type": "Feature",
            "properties": {
                "ID": 18,
                "Lat": 53.53331276,
                "Lon": 8.58049404,
                "Parter": "18 Partner",
                "Country": "Germany",
                "PI": "Charlotte Havermans",
                "Short": "AWI",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    8.58049404,
                    53.53331276
                ]
            }
        },
        {
            "id": "18",
            "type": "Feature",
            "properties": {
                "ID": 19,
                "Lat": 41.39445431,
                "Lon": 2.152505382,
                "Parter": "19 Partner",
                "Country": "Spain",
                "PI": "Michael Djaoui",
                "Short": "CROWE",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.152505382,
                    41.39445431
                ]
            }
        },
        {
            "id": "19",
            "type": "Feature",
            "properties": {
                "ID": 20,
                "Lat": 60.3999584,
                "Lon": 5.303981996,
                "Parter": "20 Partner",
                "Country": "Norway",
                "PI": "Hans Kristian Strand",
                "Short": "IMR",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    5.303981996,
                    60.3999584
                ]
            }
        },
        {
            "id": "20",
            "type": "Feature",
            "properties": {
                "ID": 21,
                "Lat": 40.42220496,
                "Lon": -3.708443403,
                "Parter": "21 Partner",
                "Country": "Spain",
                "PI": "Ricardo Aguilar",
                "Short": "OCEANA",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -3.708443403,
                    40.42220496
                ]
            }
        },
        {
            "id": "21",
            "type": "Feature",
            "properties": {
                "ID": 22,
                "Lat": 28.18145479,
                "Lon": -16.79435009,
                "Parter": "23 Partner",
                "Country": "Spain",
                "PI": "Carlos Mallo",
                "Short": "Innoceana",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.79435009,
                    28.18145479
                ]
            }
        },
        {
            "id": "22",
            "type": "Feature",
            "properties": {
                "ID": 23,
                "Lat": 41.38642486,
                "Lon": 2.177551941,
                "Parter": "23 Partner",
                "Country": "Spain",
                "PI": "Carlos Mallo",
                "Short": "Innoceana",
                "Style_URL": "./Style/T7.qml",
                "svg_URL": "./Style/T7.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.177551941,
                    41.38642486
                ]
            }
        }
    ]
};