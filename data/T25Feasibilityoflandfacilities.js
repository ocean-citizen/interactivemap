var json_T25Feasibilityoflandfacilities = {
    "type": "FeatureCollection",
    "name": "T25Feasibilityoflandfacilities",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 1,
                "Lat": 40.34957408,
                "Lon": 18.16731277,
                "Parter": "1 Coordinator",
                "Country": "Italy",
                "PI": "Sergio Rossi",
                "Short": "UNISAL",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    18.16731277,
                    40.34957408
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "ID": 2,
                "Lat": 32.82616636,
                "Lon": 34.95663232,
                "Parter": "2 Partner",
                "Country": "Israel",
                "PI": "Baruch Rinkevich",
                "Short": "IOLR",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    34.95663232,
                    32.82616636
                ]
            }
        },
        {
            "id": "2",
            "type": "Feature",
            "properties": {
                "ID": 6,
                "Lat": 41.39471975,
                "Lon": 2.153917085,
                "Parter": "6 Partner",
                "Country": "Spain",
                "PI": "Marc Garcia-Duran Huet",
                "Short": "UGI",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.153917085,
                    41.39471975
                ]
            }
        },
        {
            "id": "3",
            "type": "Feature",
            "properties": {
                "ID": 15,
                "Lat": 29.55442633,
                "Lon": 34.97036524,
                "Parter": "15 Partner",
                "Country": "Israel",
                "PI": "Julia Rozental",
                "Short": "V-CORALS",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    34.97036524,
                    29.55442633
                ]
            }
        }
    ]
};