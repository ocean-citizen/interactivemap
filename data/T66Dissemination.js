var json_T66Dissemination = {
    "type": "FeatureCollection",
    "name": "T66Dissemination",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 1,
                "Lat": 40.34957408,
                "Lon": 18.16731277,
                "Parter": "1 Coordinator",
                "Country": "Italy",
                "PI": "Sergio Rossi",
                "Short": "UNISAL",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    18.16731277,
                    40.34957408
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "ID": 4,
                "Lat": 41.9120566,
                "Lon": 12.47535532,
                "Parter": "4 Partner",
                "Country": "Italy",
                "PI": "Paolo Vassallo",
                "Short": "CONISMA",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.47535532,
                    41.9120566
                ]
            }
        },
        {
            "id": "2",
            "type": "Feature",
            "properties": {
                "ID": 6,
                "Lat": 41.39471975,
                "Lon": 2.153917085,
                "Parter": "6 Partner",
                "Country": "Spain",
                "PI": "Marc Garcia-Duran Huet",
                "Short": "UGI",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.153917085,
                    41.39471975
                ]
            }
        },
        {
            "id": "3",
            "type": "Feature",
            "properties": {
                "ID": 13,
                "Lat": 41.3885732,
                "Lon": 2.175964782,
                "Parter": "13 Partner",
                "Country": "Spain",
                "PI": "Juanita Zorrilla Pujana",
                "Short": "SUBMON",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.175964782,
                    41.3885732
                ]
            }
        },
        {
            "id": "4",
            "type": "Feature",
            "properties": {
                "ID": 19,
                "Lat": 41.39445431,
                "Lon": 2.152505382,
                "Parter": "19 Partner",
                "Country": "Spain",
                "PI": "Michael Djaoui",
                "Short": "CROWE",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.152505382,
                    41.39445431
                ]
            }
        },
        {
            "id": "5",
            "type": "Feature",
            "properties": {
                "ID": 22,
                "Lat": 28.18145479,
                "Lon": -16.79435009,
                "Parter": "23 Partner",
                "Country": "Spain",
                "PI": "Carlos Mallo",
                "Short": "Innoceana",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.79435009,
                    28.18145479
                ]
            }
        },
        {
            "id": "6",
            "type": "Feature",
            "properties": {
                "ID": 23,
                "Lat": 41.38642486,
                "Lon": 2.177551941,
                "Parter": "23 Partner",
                "Country": "Spain",
                "PI": "Carlos Mallo",
                "Short": "Innoceana",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.177551941,
                    41.38642486
                ]
            }
        }
    ]
};