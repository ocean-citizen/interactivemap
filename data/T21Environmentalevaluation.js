var json_T21Environmentalevaluation = {
    "type": "FeatureCollection",
    "name": "T21Environmentalevaluation",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 1,
                "Lat": 40.34957408,
                "Lon": 18.16731277,
                "Parter": "1 Coordinator",
                "Country": "Italy",
                "PI": "Sergio Rossi",
                "Short": "UNISAL",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    18.16731277,
                    40.34957408
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "ID": 5,
                "Lat": 41.38530625,
                "Lon": 2.196064293,
                "Parter": "5 Partner",
                "Country": "Spain",
                "PI": "Enrique Isla",
                "Short": "CSIC",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.196064293,
                    41.38530625
                ]
            }
        },
        {
            "id": "2",
            "type": "Feature",
            "properties": {
                "ID": 7,
                "Lat": 28.09909899,
                "Lon": -15.41989115,
                "Parter": "7 Partner",
                "Country": "Spain",
                "PI": "Otero Ferrer Francisco",
                "Short": "ULPGC",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -15.41989115,
                    28.09909899
                ]
            }
        },
        {
            "id": "3",
            "type": "Feature",
            "properties": {
                "ID": 8,
                "Lat": 41.38680116,
                "Lon": 2.16393047,
                "Parter": "8 Partner",
                "Country": "Spain",
                "PI": "Andrea Gori",
                "Short": "UB",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.16393047,
                    41.38680116
                ]
            }
        },
        {
            "id": "4",
            "type": "Feature",
            "properties": {
                "ID": 9,
                "Lat": 44.41150874,
                "Lon": 8.929847878,
                "Parter": "9 Partner",
                "Country": "Italy",
                "PI": "Marco Palma",
                "Short": "UBICA",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    8.929847878,
                    44.41150874
                ]
            }
        },
        {
            "id": "5",
            "type": "Feature",
            "properties": {
                "ID": 10,
                "Lat": 53.27930741,
                "Lon": -9.061184376,
                "Parter": "10 Partner",
                "Country": "Irland",
                "PI": "Louise Allcock",
                "Short": "NUIG",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -9.061184376,
                    53.27930741
                ]
            }
        },
        {
            "id": "6",
            "type": "Feature",
            "properties": {
                "ID": 11,
                "Lat": 27.99223955,
                "Lon": -15.3686539,
                "Parter": "11 Partner",
                "Country": "Spain",
                "PI": "Eric Delory",
                "Short": "PLOCAN",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -15.3686539,
                    27.99223955
                ]
            }
        },
        {
            "id": "7",
            "type": "Feature",
            "properties": {
                "ID": 14,
                "Lat": 55.78602365,
                "Lon": 12.52439728,
                "Parter": "14 Partner",
                "Country": "Danmark",
                "PI": "Patrizio Mariani",
                "Short": "DTU",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.52439728,
                    55.78602365
                ]
            }
        },
        {
            "id": "8",
            "type": "Feature",
            "properties": {
                "ID": 16,
                "Lat": 54.17939719,
                "Lon": 12.08118254,
                "Parter": "16 Partner",
                "Country": "Germany",
                "PI": "Peter Feldens",
                "Short": "IOW",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.08118254,
                    54.17939719
                ]
            }
        },
        {
            "id": "9",
            "type": "Feature",
            "properties": {
                "ID": 17,
                "Lat": 54.33989425,
                "Lon": 10.12008974,
                "Parter": "17 Partner",
                "Country": "Germany",
                "PI": "Jens Schneider von Deimling",
                "Short": "CAU",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    10.12008974,
                    54.33989425
                ]
            }
        },
        {
            "id": "10",
            "type": "Feature",
            "properties": {
                "ID": 22,
                "Lat": 28.18145479,
                "Lon": -16.79435009,
                "Parter": "23 Partner",
                "Country": "Spain",
                "PI": "Carlos Mallo",
                "Short": "Innoceana",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.79435009,
                    28.18145479
                ]
            }
        },
        {
            "id": "11",
            "type": "Feature",
            "properties": {
                "ID": 23,
                "Lat": 41.38642486,
                "Lon": 2.177551941,
                "Parter": "23 Partner",
                "Country": "Spain",
                "PI": "Carlos Mallo",
                "Short": "Innoceana",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.177551941,
                    41.38642486
                ]
            }
        }
    ]
};