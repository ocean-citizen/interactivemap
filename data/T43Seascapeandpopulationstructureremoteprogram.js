var json_T43Seascapeandpopulationstructureremoteprogram = {
    "type": "FeatureCollection",
    "name": "T43Seascapeandpopulationstructureremoteprogram",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 1,
                "Lat": 40.34957408,
                "Lon": 18.16731277,
                "Parter": "1 Coordinator",
                "Country": "Italy",
                "PI": "Sergio Rossi",
                "Short": "UNISAL",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    18.16731277,
                    40.34957408
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "ID": 2,
                "Lat": 32.82616636,
                "Lon": 34.95663232,
                "Parter": "2 Partner",
                "Country": "Israel",
                "PI": "Baruch Rinkevich",
                "Short": "IOLR",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    34.95663232,
                    32.82616636
                ]
            }
        },
        {
            "id": "2",
            "type": "Feature",
            "properties": {
                "ID": 5,
                "Lat": 41.38530625,
                "Lon": 2.196064293,
                "Parter": "5 Partner",
                "Country": "Spain",
                "PI": "Enrique Isla",
                "Short": "CSIC",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.196064293,
                    41.38530625
                ]
            }
        },
        {
            "id": "3",
            "type": "Feature",
            "properties": {
                "ID": 8,
                "Lat": 41.38680116,
                "Lon": 2.16393047,
                "Parter": "8 Partner",
                "Country": "Spain",
                "PI": "Andrea Gori",
                "Short": "UB",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.16393047,
                    41.38680116
                ]
            }
        },
        {
            "id": "4",
            "type": "Feature",
            "properties": {
                "ID": 11,
                "Lat": 27.99223955,
                "Lon": -15.3686539,
                "Parter": "11 Partner",
                "Country": "Spain",
                "PI": "Eric Delory",
                "Short": "PLOCAN",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -15.3686539,
                    27.99223955
                ]
            }
        },
        {
            "id": "5",
            "type": "Feature",
            "properties": {
                "ID": 16,
                "Lat": 54.17939719,
                "Lon": 12.08118254,
                "Parter": "16 Partner",
                "Country": "Germany",
                "PI": "Peter Feldens",
                "Short": "IOW",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.08118254,
                    54.17939719
                ]
            }
        },
        {
            "id": "6",
            "type": "Feature",
            "properties": {
                "ID": 17,
                "Lat": 54.33989425,
                "Lon": 10.12008974,
                "Parter": "17 Partner",
                "Country": "Germany",
                "PI": "Jens Schneider von Deimling",
                "Short": "CAU",
                "Style_URL": "./Style/T4.qml",
                "svg_URL": "./Style/T4.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    10.12008974,
                    54.33989425
                ]
            }
        }
    ]
};