var json_CSIS2023LanderDeploment = {
    "type": "FeatureCollection",
    "name": "CSIS2023LanderDeploment",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 5,
                "URL_Img": "CSIS2023LanderDeploment/CSIS - 2023 - Lander Deployment.png",
                "Operator": "CSIS",
                "Intervention Area": "TEN",
                "Operation Period": "3000 - 3001",
                "Sampling Frequnecy": "any value",
                "Related Task": "T2.1, T4",
                "Status": "OnGoing",
                "Action": "Mooring",
                "Sensor Type": "X1, X2, Y3, Z4",
                "Oceanic Variable": "Current, Temp, Salinity",
                "Short Discription": "Lander opperation with multiple sensors",
                "Person of Contact": "Enrique Isla",
                "Availability": "example",
                "Comments": "example",
                "Layer Name": "CSIS - 2023 - Lander Deploment",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023-09-12_TEN_LanderDeployment.pdf\" target=\"_blank\">OC_2023-09-12_TEN_LanderDeployment.pdf</a>",
                "URL_Style": "Point_OnGoing.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8405,
                    28.2077
                ]
            }
        }
    ]
};