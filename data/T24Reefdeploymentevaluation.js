var json_T24Reefdeploymentevaluation = {
    "type": "FeatureCollection",
    "name": "T24Reefdeploymentevaluation",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 1,
                "Lat": 40.34957408,
                "Lon": 18.16731277,
                "Parter": "1 Coordinator",
                "Country": "Italy",
                "PI": "Sergio Rossi",
                "Short": "UNISAL",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    18.16731277,
                    40.34957408
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "ID": 4,
                "Lat": 41.9120566,
                "Lon": 12.47535532,
                "Parter": "4 Partner",
                "Country": "Italy",
                "PI": "Paolo Vassallo",
                "Short": "CONISMA",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.47535532,
                    41.9120566
                ]
            }
        },
        {
            "id": "2",
            "type": "Feature",
            "properties": {
                "ID": 5,
                "Lat": 41.38530625,
                "Lon": 2.196064293,
                "Parter": "5 Partner",
                "Country": "Spain",
                "PI": "Enrique Isla",
                "Short": "CSIC",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.196064293,
                    41.38530625
                ]
            }
        },
        {
            "id": "3",
            "type": "Feature",
            "properties": {
                "ID": 6,
                "Lat": 41.39471975,
                "Lon": 2.153917085,
                "Parter": "6 Partner",
                "Country": "Spain",
                "PI": "Marc Garcia-Duran Huet",
                "Short": "UGI",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.153917085,
                    41.39471975
                ]
            }
        },
        {
            "id": "4",
            "type": "Feature",
            "properties": {
                "ID": 10,
                "Lat": 53.27930741,
                "Lon": -9.061184376,
                "Parter": "10 Partner",
                "Country": "Irland",
                "PI": "Louise Allcock",
                "Short": "NUIG",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -9.061184376,
                    53.27930741
                ]
            }
        },
        {
            "id": "5",
            "type": "Feature",
            "properties": {
                "ID": 21,
                "Lat": 40.42220496,
                "Lon": -3.708443403,
                "Parter": "21 Partner",
                "Country": "Spain",
                "PI": "Ricardo Aguilar",
                "Short": "OCEANA",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -3.708443403,
                    40.42220496
                ]
            }
        }
    ]
};