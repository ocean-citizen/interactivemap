var json_T37Informationstrategyforstakeholders = {
    "type": "FeatureCollection",
    "name": "T37Informationstrategyforstakeholders",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 1,
                "Lat": 40.34957408,
                "Lon": 18.16731277,
                "Parter": "1 Coordinator",
                "Country": "Italy",
                "PI": "Sergio Rossi",
                "Short": "UNISAL",
                "Style_URL": "./Style/T3.qml",
                "svg_URL": "./Style/T3.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    18.16731277,
                    40.34957408
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "ID": 6,
                "Lat": 41.39471975,
                "Lon": 2.153917085,
                "Parter": "6 Partner",
                "Country": "Spain",
                "PI": "Marc Garcia-Duran Huet",
                "Short": "UGI",
                "Style_URL": "./Style/T3.qml",
                "svg_URL": "./Style/T3.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.153917085,
                    41.39471975
                ]
            }
        },
        {
            "id": "2",
            "type": "Feature",
            "properties": {
                "ID": 14,
                "Lat": 55.78602365,
                "Lon": 12.52439728,
                "Parter": "14 Partner",
                "Country": "Danmark",
                "PI": "Patrizio Mariani",
                "Short": "DTU",
                "Style_URL": "./Style/T3.qml",
                "svg_URL": "./Style/T3.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.52439728,
                    55.78602365
                ]
            }
        },
        {
            "id": "3",
            "type": "Feature",
            "properties": {
                "ID": 15,
                "Lat": 29.55442633,
                "Lon": 34.97036524,
                "Parter": "15 Partner",
                "Country": "Israel",
                "PI": "Julia Rozental",
                "Short": "V-CORALS",
                "Style_URL": "./Style/T3.qml",
                "svg_URL": "./Style/T3.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    34.97036524,
                    29.55442633
                ]
            }
        },
        {
            "id": "4",
            "type": "Feature",
            "properties": {
                "ID": 22,
                "Lat": 28.18145479,
                "Lon": -16.79435009,
                "Parter": "23 Partner",
                "Country": "Spain",
                "PI": "Carlos Mallo",
                "Short": "Innoceana",
                "Style_URL": "./Style/T3.qml",
                "svg_URL": "./Style/T3.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.79435009,
                    28.18145479
                ]
            }
        },
        {
            "id": "5",
            "type": "Feature",
            "properties": {
                "ID": 23,
                "Lat": 41.38642486,
                "Lon": 2.177551941,
                "Parter": "23 Partner",
                "Country": "Spain",
                "PI": "Carlos Mallo",
                "Short": "Innoceana",
                "Style_URL": "./Style/T3.qml",
                "svg_URL": "./Style/T3.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.177551941,
                    41.38642486
                ]
            }
        }
    ]
};