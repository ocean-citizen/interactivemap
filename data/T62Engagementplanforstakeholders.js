var json_T62Engagementplanforstakeholders = {
    "type": "FeatureCollection",
    "name": "T62Engagementplanforstakeholders",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 1,
                "Lat": 40.34957408,
                "Lon": 18.16731277,
                "Parter": "1 Coordinator",
                "Country": "Italy",
                "PI": "Sergio Rossi",
                "Short": "UNISAL",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    18.16731277,
                    40.34957408
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "ID": 2,
                "Lat": 32.82616636,
                "Lon": 34.95663232,
                "Parter": "2 Partner",
                "Country": "Israel",
                "PI": "Baruch Rinkevich",
                "Short": "IOLR",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    34.95663232,
                    32.82616636
                ]
            }
        },
        {
            "id": "2",
            "type": "Feature",
            "properties": {
                "ID": 6,
                "Lat": 41.39471975,
                "Lon": 2.153917085,
                "Parter": "6 Partner",
                "Country": "Spain",
                "PI": "Marc Garcia-Duran Huet",
                "Short": "UGI",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.153917085,
                    41.39471975
                ]
            }
        },
        {
            "id": "3",
            "type": "Feature",
            "properties": {
                "ID": 8,
                "Lat": 41.38680116,
                "Lon": 2.16393047,
                "Parter": "8 Partner",
                "Country": "Spain",
                "PI": "Andrea Gori",
                "Short": "UB",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.16393047,
                    41.38680116
                ]
            }
        },
        {
            "id": "4",
            "type": "Feature",
            "properties": {
                "ID": 14,
                "Lat": 55.78602365,
                "Lon": 12.52439728,
                "Parter": "14 Partner",
                "Country": "Danmark",
                "PI": "Patrizio Mariani",
                "Short": "DTU",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.52439728,
                    55.78602365
                ]
            }
        },
        {
            "id": "5",
            "type": "Feature",
            "properties": {
                "ID": 15,
                "Lat": 29.55442633,
                "Lon": 34.97036524,
                "Parter": "15 Partner",
                "Country": "Israel",
                "PI": "Julia Rozental",
                "Short": "V-CORALS",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    34.97036524,
                    29.55442633
                ]
            }
        },
        {
            "id": "6",
            "type": "Feature",
            "properties": {
                "ID": 22,
                "Lat": 28.18145479,
                "Lon": -16.79435009,
                "Parter": "23 Partner",
                "Country": "Spain",
                "PI": "Carlos Mallo",
                "Short": "Innoceana",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.79435009,
                    28.18145479
                ]
            }
        },
        {
            "id": "7",
            "type": "Feature",
            "properties": {
                "ID": 23,
                "Lat": 41.38642486,
                "Lon": 2.177551941,
                "Parter": "23 Partner",
                "Country": "Spain",
                "PI": "Carlos Mallo",
                "Short": "Innoceana",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.177551941,
                    41.38642486
                ]
            }
        }
    ]
};