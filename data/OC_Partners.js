var json_OC_Partners = {
    "type": "FeatureCollection",
    "name": "OC_Partners",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/UNISAL.png",
                "Organisation": "University del Salento",
                "Short": "UNISAL",
                "Country": "Italy",
                "Partner": "1 Coordinator",
                "PI": "Sergio Rossi",
                "Lead": "WP7, T2.5, T3.6, T4.5, T4.7, T6.7, T7.2, T7.3",
                "Participants": "T1.1, T1.2, T2.1, T2.2, T2.3, T2.4, T2.6, T3.1, T3.2, T3.3, T3.4, T3.5, T3.7, T4.1, T4.2, T4.3, T4.4, T4.9, T5.2, T6.1, T6.2, T6.3, T6.4, T6.5, T6.6, T7.1, T7.4",
                "Lat": 40.34957408,
                "Lon": 18.16731277
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    18.16731277,
                    40.34957408
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/IOLR.png",
                "Organisation": "Israel Oceanographic and Limnological Research Limited",
                "Short": "IOLR",
                "Country": "Israel",
                "Partner": "2",
                "PI": "Baruch Rinkevich",
                "Lead": "WP3",
                "Participants": "T2.5, T3.1, T3.2, T3.3, T3.4, T3.5, T4.3, T4.4, T4.5, T5.2, T6.1, T6.2, T6.3, T6.4, T6.5, T6.7, T7.1, T7.2, T7.3, T7.4",
                "Lat": 32.82616636,
                "Lon": 34.95663232
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    34.95663232,
                    32.82616636
                ]
            }
        },
        {
            "id": "2",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/CNRS.png",
                "Organisation": "Sorbonne University, Centre National de la Recherche Scientifique",
                "Short": "CNRS",
                "Country": "France",
                "Partner": "3",
                "PI": "Lorenzo Bramanti",
                "Lead": "WP2, T2.2",
                "Participants": "T2.3, T4.1, T4.2, T4.6, T7.1, T7.2, T7.3, T7.4",
                "Lat": 48.84775399,
                "Lon": 2.264089621
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.264089621,
                    48.84775399
                ]
            }
        },
        {
            "id": "3",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/CoNISMa.png",
                "Organisation": "Consorzio Nazionale Interuniversitario per le Scienze del Mare Associazione",
                "Short": "CoNISMa",
                "Country": "Italy",
                "Partner": "4",
                "PI": "Paolo Vassallo",
                "Lead": "WP2, WP5, T2.3, T3.1, T3.2, T3.3, T3.7, T4.2, T5.1, T5.2, T5.4",
                "Participants": "T2.2, T2.4, T3.4, T3.5, T4.1, T4.9, T5.3, T6.3, T6.4, T6.5, T6.6, T7.1, T7.2, T7.3, T7.4",
                "Lat": 41.9120566,
                "Lon": 12.47535532
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.47535532,
                    41.9120566
                ]
            }
        },
        {
            "id": "4",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/CSIC.png",
                "Organisation": "Agencia Estatal Consejo Superior de Investigaciones Cientificas",
                "Short": "CSIC",
                "Country": "Spain",
                "Partner": "5",
                "PI": "Enrique Isla",
                "Lead": "WP3, WP4, T3.4, T4.1, T4.4",
                "Participants": "T2.1, T2.2, T2.4, T3.1, T3.3, T4.2, T4.3, T4.9, T7.1, T7.2, T7.3, T7.4",
                "Lat": 41.38530625,
                "Lon": 2.196064293
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.196064293,
                    41.38530625
                ]
            }
        },
        {
            "id": "5",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/UGI.png",
                "Organisation": "Underwater Gardens International, S.L.",
                "Short": "UGI",
                "Country": "Spain",
                "Partner": "6",
                "PI": "Marc Garcia-Duran Huet",
                "Lead": "WP1, WP5, WP6, T1.3, T2.4, T5.3, T6.1, T6.2, T6.6",
                "Participants": "T2.5, T2.6, T3.1, T3.5, T3.6, T4.5, T4.7, T4.8, T5.1, T5.2, T5.4, T6.3, T6.4, T6.5, T7.1, T7.2, T7.3, T7.4",
                "Lat": 41.39471975,
                "Lon": 2.153917085
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.153917085,
                    41.39471975
                ]
            }
        },
        {
            "id": "6",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/ULPGC.png",
                "Organisation": "Universidad de las Palmas de Gran Canaria",
                "Short": "ULPGC",
                "Country": "Spain",
                "Partner": "7",
                "PI": "Otero Ferrer Francisco",
                "Lead": "T2.6",
                "Participants": "T1.3, T2.1, T2.2, T2.3, T3.1, T3.2, T3.3, T3.4, T3.5, T3.6, T4.5, T4.7, T4.8, T5.1, T5.2, T5.3, T6.1, T6.3, T6.4, T6.5, T7.1, T7.2, T7.3, T7.4",
                "Lat": 28.09909899,
                "Lon": -15.41989115
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -15.41989115,
                    28.09909899
                ]
            }
        },
        {
            "id": "7",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/UB.png",
                "Organisation": "Universitat de Barcelona",
                "Short": "UB",
                "Country": "Spain",
                "Partner": "8",
                "PI": "Andrea Gori",
                "Lead": "T3.5",
                "Participants": "T1.1, T1.3, T2.1, T2.2, T3.1, T3.4, T4.1, T4.2, T4.3, T4.4, T4.6, T4.8, T6.1, T6.2, T6.3, T6.4, T6.5, T7.1, T7.2, T7.3, T7.4",
                "Lat": 41.38680116,
                "Lon": 2.16393047
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.16393047,
                    41.38680116
                ]
            }
        },
        {
            "id": "8",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/UBICA.png",
                "Organisation": "Underwater Bio-Cartography SRL",
                "Short": "UBICA",
                "Country": "Italy",
                "Partner": "9",
                "PI": "Marco Palma",
                "Lead": null,
                "Participants": "T2.1, T3.1, T3.2, T4.4, T4.5, T4.8, T7.1, T7.2, T7.3, T7.4",
                "Lat": 44.41150874,
                "Lon": 8.929847878
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    8.929847878,
                    44.41150874
                ]
            }
        },
        {
            "id": "9",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/NUIG.png",
                "Organisation": "National University of Ireland, Universtiy Galway",
                "Short": "NUIG",
                "Country": "Irland",
                "Partner": "10",
                "PI": "Louise Allcock",
                "Lead": "T4.6",
                "Participants": "T2.1, T2.2, T2.4, T3.5, T4.2, T7.1, T7.2, T7.3, T7.4",
                "Lat": 53.27930741,
                "Lon": -9.061184376
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -9.061184376,
                    53.27930741
                ]
            }
        },
        {
            "id": "10",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/PLOCAN.png",
                "Organisation": "Oceanic Platform of the Canary Islands",
                "Short": "PLOCAN",
                "Country": "Spain",
                "Partner": "11",
                "PI": "Eric Delory",
                "Lead": "WP4, T4.8",
                "Participants": "T2.1, T4.1, T4.3, T4.7, T6.3, T6.4, T7.1, T7.2, T7.3, T7.4",
                "Lat": 27.99223955,
                "Lon": -15.3686539
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -15.3686539,
                    27.99223955
                ]
            }
        },
        {
            "id": "11",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/UNEXE.png",
                "Organisation": "University of Exeter",
                "Short": "UNEXE",
                "Country": "United Kingdom",
                "Partner": "12",
                "PI": "Ruth Thurstan",
                "Lead": "WP1, T1.1,T1.2",
                "Participants": "T1.3, T7.1, T7.2, T7.3, T7.4",
                "Lat": 50.7369236,
                "Lon": -3.534859647
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -3.534859647,
                    50.7369236
                ]
            }
        },
        {
            "id": "12",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/SUBMON.png",
                "Organisation": "SUBMON",
                "Short": "SUBMON",
                "Country": "Spain",
                "Partner": "13",
                "PI": "Juanita Zorrilla Pujana",
                "Lead": "WP6, T6.3, T6.4, T6.5",
                "Participants": "T6.6, T7.1, T7.2, T7.3, T7.4",
                "Lat": 41.3885732,
                "Lon": 2.175964782
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.175964782,
                    41.3885732
                ]
            }
        },
        {
            "id": "13",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/DTU.png",
                "Organisation": "Technical University of Denmark",
                "Short": "DTU",
                "Country": "Danmark",
                "Partner": "14",
                "PI": "Patrizio Mariani",
                "Lead": null,
                "Participants": "T2.1, T2.2, T3.1, T3.2, T3.4, T3.6, T3.7, T4.1, T4.2, T4.6, T6.2, T6.3, T6.4, T6.5, T7.1, T7.2, T7.3, T7.4",
                "Lat": 55.78602365,
                "Lon": 12.52439728
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.52439728,
                    55.78602365
                ]
            }
        },
        {
            "id": "14",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/V-CORALS.png",
                "Organisation": "V-Corals",
                "Short": "V-CORALS",
                "Country": "Israel",
                "Partner": "15",
                "PI": "Julia Rozental",
                "Lead": null,
                "Participants": "T1.1, T2.5, T3.3, T3.7, T6.2, T7.1, T7.2, T7.3, T7.4",
                "Lat": 29.55442633,
                "Lon": 34.97036524
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    34.97036524,
                    29.55442633
                ]
            }
        },
        {
            "id": "15",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/IOW.png",
                "Organisation": "Leibniz Institute for Baltic Sea Research Warnemunde",
                "Short": "IOW",
                "Country": "Germany",
                "Partner": "16",
                "PI": "Peter Feldens",
                "Lead": "T2.1, T4.3",
                "Participants": "T4.1, T4.8, T4.9, T6.3, T6.4, T7.1, T7.2, T7.3, T7.4",
                "Lat": 54.17939719,
                "Lon": 12.08118254
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.08118254,
                    54.17939719
                ]
            }
        },
        {
            "id": "16",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/CAU.png",
                "Organisation": "Christian-Albrechts-University",
                "Short": "CAU",
                "Country": "Germany",
                "Partner": "17",
                "PI": "Jens Schneider von Deimling",
                "Lead": null,
                "Participants": "T2.1, T4.1, T4.3, T4.8, T4.9, T6.3, T6.4, T7.1, T7.2, T7.3, T7.4",
                "Lat": 54.33989425,
                "Lon": 10.12008974
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    10.12008974,
                    54.33989425
                ]
            }
        },
        {
            "id": "17",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/AWI.png",
                "Organisation": "Alfred Wegener Institute",
                "Short": "AWI",
                "Country": "Germany",
                "Partner": "18",
                "PI": "Charlotte Havermans",
                "Lead": null,
                "Participants": "T2.2, T2.3, T4.2, T7.1, T7.2, T7.3, T7.4",
                "Lat": 53.53331276,
                "Lon": 8.58049404
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    8.58049404,
                    53.53331276
                ]
            }
        },
        {
            "id": "18",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/Crowe.png",
                "Organisation": "Crowe Spain",
                "Short": "Crowe",
                "Country": "Spain",
                "Partner": "19",
                "PI": "Michael Djaoui",
                "Lead": "WP7, T7.1, T7.4",
                "Participants": "T6.1, T6.3, T6.6, T7.2, T7.3",
                "Lat": 41.39445431,
                "Lon": 2.152505382
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.152505382,
                    41.39445431
                ]
            }
        },
        {
            "id": "19",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/IMR.png",
                "Organisation": "Institute of Marine Research",
                "Short": "IMR",
                "Country": "Norway",
                "Partner": "20",
                "PI": "Hans Kristian Strand",
                "Lead": null,
                "Participants": "T1.1, T4.2, T4.4, T4.6, T7.1, T7.2, T7.3, T7.4",
                "Lat": 60.3999584,
                "Lon": 5.303981996
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    5.303981996,
                    60.3999584
                ]
            }
        },
        {
            "id": "20",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/OCEANA.png",
                "Organisation": "Foundation OCEANA",
                "Short": "OCEANA",
                "Country": "Spain",
                "Partner": "21",
                "PI": "Ricardo Aguilar",
                "Lead": null,
                "Participants": "T2.2, T2.4, T7.1, T7.2, T7.3, T7.4",
                "Lat": 40.42220496,
                "Lon": -3.708443403
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -3.708443403,
                    40.42220496
                ]
            }
        },
        {
            "id": "21",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/Innoceana.png",
                "Organisation": "INNOCEANA",
                "Short": "Innoceana",
                "Country": "Spain",
                "Partner": "23",
                "PI": "Carlos Mallo",
                "Lead": "T4.9",
                "Participants": "T2.1, T3.5, T3.7, T4.2, T5.3, T6.2, T6.6, T7.1, T7.2, T7.3, T7.4",
                "Lat": 28.18145479,
                "Lon": -16.79435009
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.79435009,
                    28.18145479
                ]
            }
        },
        {
            "id": "22",
            "type": "Feature",
            "properties": {
                "URL_Img": "/Partner_Logos/Innoceana.png",
                "Organisation": "INNOCEANA",
                "Short": "Innoceana",
                "Country": "Spain",
                "Partner": "23",
                "PI": "Carlos Mallo",
                "Lead": "T4.9",
                "Participants": "T2.1, T3.5, T3.7, T4.2, T5.3, T6.2, T6.6, T7.1, T7.2, T7.3, T7.4",
                "Lat": 41.38642486,
                "Lon": 2.177551941
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.177551941,
                    41.38642486
                ]
            }
        }
    ]
};