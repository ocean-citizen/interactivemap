var json_T23Connectivity = {
    "type": "FeatureCollection",
    "name": "T23Connectivity",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 1,
                "Lat": 40.34957408,
                "Lon": 18.16731277,
                "Parter": "1 Coordinator",
                "Country": "Italy",
                "PI": "Sergio Rossi",
                "Short": "UNISAL",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    18.16731277,
                    40.34957408
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "Lat": 48.84775399,
                "Lon": 2.264089621,
                "Parter": "3 Partner",
                "Country": "France",
                "PI": "Lorenzo Bramanti",
                "Short": "CNRS",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.264089621,
                    48.84775399
                ]
            }
        },
        {
            "id": "2",
            "type": "Feature",
            "properties": {
                "ID": 4,
                "Lat": 41.9120566,
                "Lon": 12.47535532,
                "Parter": "4 Partner",
                "Country": "Italy",
                "PI": "Paolo Vassallo",
                "Short": "CONISMA",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.47535532,
                    41.9120566
                ]
            }
        },
        {
            "id": "3",
            "type": "Feature",
            "properties": {
                "ID": 7,
                "Lat": 28.09909899,
                "Lon": -15.41989115,
                "Parter": "7 Partner",
                "Country": "Spain",
                "PI": "Otero Ferrer Francisco",
                "Short": "ULPGC",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -15.41989115,
                    28.09909899
                ]
            }
        },
        {
            "id": "4",
            "type": "Feature",
            "properties": {
                "ID": 18,
                "Lat": 53.53331276,
                "Lon": 8.58049404,
                "Parter": "18 Partner",
                "Country": "Germany",
                "PI": "Charlotte Havermans",
                "Short": "AWI",
                "Style_URL": "./Style/T2.qml",
                "svg_URL": "./Style/T2.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    8.58049404,
                    53.53331276
                ]
            }
        }
    ]
};