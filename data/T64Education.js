var json_T64Education = {
    "type": "FeatureCollection",
    "name": "T64Education",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 1,
                "Lat": 40.34957408,
                "Lon": 18.16731277,
                "Parter": "1 Coordinator",
                "Country": "Italy",
                "PI": "Sergio Rossi",
                "Short": "UNISAL",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    18.16731277,
                    40.34957408
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "ID": 2,
                "Lat": 32.82616636,
                "Lon": 34.95663232,
                "Parter": "2 Partner",
                "Country": "Israel",
                "PI": "Baruch Rinkevich",
                "Short": "IOLR",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    34.95663232,
                    32.82616636
                ]
            }
        },
        {
            "id": "2",
            "type": "Feature",
            "properties": {
                "ID": 4,
                "Lat": 41.9120566,
                "Lon": 12.47535532,
                "Parter": "4 Partner",
                "Country": "Italy",
                "PI": "Paolo Vassallo",
                "Short": "CONISMA",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.47535532,
                    41.9120566
                ]
            }
        },
        {
            "id": "3",
            "type": "Feature",
            "properties": {
                "ID": 6,
                "Lat": 41.39471975,
                "Lon": 2.153917085,
                "Parter": "6 Partner",
                "Country": "Spain",
                "PI": "Marc Garcia-Duran Huet",
                "Short": "UGI",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.153917085,
                    41.39471975
                ]
            }
        },
        {
            "id": "4",
            "type": "Feature",
            "properties": {
                "ID": 7,
                "Lat": 28.09909899,
                "Lon": -15.41989115,
                "Parter": "7 Partner",
                "Country": "Spain",
                "PI": "Otero Ferrer Francisco",
                "Short": "ULPGC",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -15.41989115,
                    28.09909899
                ]
            }
        },
        {
            "id": "5",
            "type": "Feature",
            "properties": {
                "ID": 8,
                "Lat": 41.38680116,
                "Lon": 2.16393047,
                "Parter": "8 Partner",
                "Country": "Spain",
                "PI": "Andrea Gori",
                "Short": "UB",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.16393047,
                    41.38680116
                ]
            }
        },
        {
            "id": "6",
            "type": "Feature",
            "properties": {
                "ID": 11,
                "Lat": 27.99223955,
                "Lon": -15.3686539,
                "Parter": "11 Partner",
                "Country": "Spain",
                "PI": "Eric Delory",
                "Short": "PLOCAN",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -15.3686539,
                    27.99223955
                ]
            }
        },
        {
            "id": "7",
            "type": "Feature",
            "properties": {
                "ID": 13,
                "Lat": 41.3885732,
                "Lon": 2.175964782,
                "Parter": "13 Partner",
                "Country": "Spain",
                "PI": "Juanita Zorrilla Pujana",
                "Short": "SUBMON",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.175964782,
                    41.3885732
                ]
            }
        },
        {
            "id": "8",
            "type": "Feature",
            "properties": {
                "ID": 14,
                "Lat": 55.78602365,
                "Lon": 12.52439728,
                "Parter": "14 Partner",
                "Country": "Danmark",
                "PI": "Patrizio Mariani",
                "Short": "DTU",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.52439728,
                    55.78602365
                ]
            }
        },
        {
            "id": "9",
            "type": "Feature",
            "properties": {
                "ID": 16,
                "Lat": 54.17939719,
                "Lon": 12.08118254,
                "Parter": "16 Partner",
                "Country": "Germany",
                "PI": "Peter Feldens",
                "Short": "IOW",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.08118254,
                    54.17939719
                ]
            }
        },
        {
            "id": "10",
            "type": "Feature",
            "properties": {
                "ID": 17,
                "Lat": 54.33989425,
                "Lon": 10.12008974,
                "Parter": "17 Partner",
                "Country": "Germany",
                "PI": "Jens Schneider von Deimling",
                "Short": "CAU",
                "Style_URL": "./Style/T6.qml",
                "svg_URL": "./Style/T6.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    10.12008974,
                    54.33989425
                ]
            }
        }
    ]
};