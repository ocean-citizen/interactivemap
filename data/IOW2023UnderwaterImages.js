var json_IOW2023UnderwaterImages = {
    "type": "FeatureCollection",
    "name": "IOW2023UnderwaterImages",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-10_G0127149.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.847017774618,
                    28.2062622115471
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-10_G0127171.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.846968885129,
                    28.2061471056472
                ]
            }
        },
        {
            "id": "2",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-10_G0127183.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8469422181374,
                    28.2061017725091
                ]
            }
        },
        {
            "id": "3",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-10_G0127192.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8469222179084,
                    28.2060677726519
                ]
            }
        },
        {
            "id": "4",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-10_G0127214.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8468733285131,
                    28.2059846618768
                ]
            }
        },
        {
            "id": "5",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-10_G0127275.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8467377719491,
                    28.2057542182676
                ]
            }
        },
        {
            "id": "6",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-10_G0127314.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8466511049426,
                    28.2056068853937
                ]
            }
        },
        {
            "id": "7",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-11_G0137763.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8482774037342,
                    28.2073322113566
                ]
            }
        },
        {
            "id": "8",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-11_G0137822.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8482632209861,
                    28.2072896631633
                ]
            }
        },
        {
            "id": "9",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-11_G0137870.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8482516824876,
                    28.2072550476827
                ]
            }
        },
        {
            "id": "10",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-11_G0137940.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8482348555238,
                    28.2072045667715
                ]
            }
        },
        {
            "id": "11",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-1_G0014161.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.839579545453,
                    28.2063079545514
                ]
            }
        },
        {
            "id": "12",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-1_G0014164.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8395772727256,
                    28.2063077272791
                ]
            }
        },
        {
            "id": "13",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-1_G0014205.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8395462121189,
                    28.2063046212212
                ]
            }
        },
        {
            "id": "14",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-2_G0024333.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8423248822445,
                    28.208049764566
                ]
            }
        },
        {
            "id": "15",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-2_G0024336.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8423192484213,
                    28.2080384969067
                ]
            }
        },
        {
            "id": "16",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-2_G0024341.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8423098587187,
                    28.2080197174739
                ]
            }
        },
        {
            "id": "17",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-2_G0024359.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8422760558024,
                    28.207964083808
                ]
            }
        },
        {
            "id": "18",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-3_G0034635.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8382243477038,
                    28.201548695684
                ]
            }
        },
        {
            "id": "19",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-3_G0034640.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8382113453069,
                    28.2015226907486
                ]
            }
        },
        {
            "id": "20",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-3_G0034666.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8381437327577,
                    28.2014437333033
                ]
            }
        },
        {
            "id": "21",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-3_G0034706.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8380397135707,
                    28.2013397149075
                ]
            }
        },
        {
            "id": "22",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-4_G0044996.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8355574324156,
                    28.2015425676163
                ]
            }
        },
        {
            "id": "23",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-4_G0044998.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8355533783581,
                    28.2015466216743
                ]
            }
        },
        {
            "id": "24",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-4_G0045013.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8355229729177,
                    28.2015770271053
                ]
            }
        },
        {
            "id": "25",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-5_G0065252.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.840290565982,
                    28.2077339622703
                ]
            }
        },
        {
            "id": "26",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-5_G0065253.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8402900942814,
                    28.2077306603838
                ]
            }
        },
        {
            "id": "27",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-5_G0065258.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8402877357789,
                    28.2077141509512
                ]
            }
        },
        {
            "id": "28",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-5_G0065304.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8402660375896,
                    28.2075622641672
                ]
            }
        },
        {
            "id": "29",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-5_G0065330.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8402537734229,
                    28.2074764151123
                ]
            }
        },
        {
            "id": "30",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-6_G0095873.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8403444949247,
                    28.2034444950293
                ]
            }
        },
        {
            "id": "31",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-6_G0095880.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8403091735431,
                    28.2034091736724
                ]
            }
        },
        {
            "id": "32",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-6_G0095881.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8403041276333,
                    28.2034041277635
                ]
            }
        },
        {
            "id": "33",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-6_G0095884.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8402889899068,
                    28.2033889900356
                ]
            }
        },
        {
            "id": "34",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-6_G0095903.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8401931177369,
                    28.2032938825008
                ]
            }
        },
        {
            "id": "35",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-6_G0095990.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8397541260129,
                    28.2029036698081
                ]
            }
        },
        {
            "id": "36",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-6_G0095995.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8397288967004,
                    28.2028812437478
                ]
            }
        },
        {
            "id": "37",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-6_G0096077.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8393151374788,
                    28.2025134556849
                ]
            }
        },
        {
            "id": "38",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-7_G0075538.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8340022980279,
                    28.1967045960651
                ]
            }
        },
        {
            "id": "39",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-7_G0075553.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8339807461755,
                    28.1966566792698
                ]
            }
        },
        {
            "id": "40",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-7_G0075624.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8338787343405,
                    28.1964271539949
                ]
            }
        },
        {
            "id": "41",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-8_G0106177.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8401528480642,
                    28.1977240936399
                ]
            }
        },
        {
            "id": "42",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-8_G0106201.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8401031067357,
                    28.1976308294799
                ]
            }
        },
        {
            "id": "43",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-8_G0106211.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8400823812077,
                    28.1975919694075
                ]
            }
        },
        {
            "id": "44",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-8_G0106328.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8398398936435,
                    28.197137306311
                ]
            }
        },
        {
            "id": "45",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-9_G0116615.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8452531922616,
                    28.2049042594933
                ]
            }
        },
        {
            "id": "46",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-9_G0116638.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.8451969887307,
                    28.2048293224238
                ]
            }
        },
        {
            "id": "47",
            "type": "Feature",
            "properties": {
                "ID": 3,
                "URL_Img": "IOW2023UnderwaterImages/Station-9_G0116712.JPG",
                "Operator": "IOW, CAU",
                "Intervention Area": "TEN",
                "Operation Period": "10.09.2023 - 13.09.2023 ",
                "Sampling Frequnecy": null,
                "Related Task": "T2.1",
                "Status": "Completed",
                "Action": "Video Survey",
                "Sensor Type": "GoPro 7",
                "Oceanic Variable": "Ground - Truthing",
                "Short Discription": "Comparison of acoustic and optical changes on the seabed",
                "Person of Contact": "Mischa Schoenke",
                "Availability": "yes",
                "Comments": "3.5 Gb of images available ",
                "Layer Name": "IOW - 2023 - Underwater Images",
                "Attachment_PDF": "<a href=\"pdf_attachments/OC_2023_TEN_Ground-Truthing.pdf\" target=\"_blank\">OC_2023_TEN_Ground-Truthing.pdf</a>",
                "URL_Style": "MultiPoint_Completed.qml"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -16.845016160511,
                    28.2045882203857
                ]
            }
        }
    ]
};