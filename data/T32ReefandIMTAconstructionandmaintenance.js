var json_T32ReefandIMTAconstructionandmaintenance = {
    "type": "FeatureCollection",
    "name": "T32ReefandIMTAconstructionandmaintenance",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 1,
                "Lat": 40.34957408,
                "Lon": 18.16731277,
                "Parter": "1 Coordinator",
                "Country": "Italy",
                "PI": "Sergio Rossi",
                "Short": "UNISAL",
                "Style_URL": "./Style/T3.qml",
                "svg_URL": "./Style/T3.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    18.16731277,
                    40.34957408
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "ID": 2,
                "Lat": 32.82616636,
                "Lon": 34.95663232,
                "Parter": "2 Partner",
                "Country": "Israel",
                "PI": "Baruch Rinkevich",
                "Short": "IOLR",
                "Style_URL": "./Style/T3.qml",
                "svg_URL": "./Style/T3.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    34.95663232,
                    32.82616636
                ]
            }
        },
        {
            "id": "2",
            "type": "Feature",
            "properties": {
                "ID": 6,
                "Lat": 41.39471975,
                "Lon": 2.153917085,
                "Parter": "6 Partner",
                "Country": "Spain",
                "PI": "Marc Garcia-Duran Huet",
                "Short": "UGI",
                "Style_URL": "./Style/T3.qml",
                "svg_URL": "./Style/T3.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.153917085,
                    41.39471975
                ]
            }
        },
        {
            "id": "3",
            "type": "Feature",
            "properties": {
                "ID": 7,
                "Lat": 28.09909899,
                "Lon": -15.41989115,
                "Parter": "7 Partner",
                "Country": "Spain",
                "PI": "Otero Ferrer Francisco",
                "Short": "ULPGC",
                "Style_URL": "./Style/T3.qml",
                "svg_URL": "./Style/T3.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -15.41989115,
                    28.09909899
                ]
            }
        },
        {
            "id": "4",
            "type": "Feature",
            "properties": {
                "ID": 9,
                "Lat": 44.41150874,
                "Lon": 8.929847878,
                "Parter": "9 Partner",
                "Country": "Italy",
                "PI": "Marco Palma",
                "Short": "UBICA",
                "Style_URL": "./Style/T3.qml",
                "svg_URL": "./Style/T3.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    8.929847878,
                    44.41150874
                ]
            }
        },
        {
            "id": "5",
            "type": "Feature",
            "properties": {
                "ID": 14,
                "Lat": 55.78602365,
                "Lon": 12.52439728,
                "Parter": "14 Partner",
                "Country": "Danmark",
                "PI": "Patrizio Mariani",
                "Short": "DTU",
                "Style_URL": "./Style/T3.qml",
                "svg_URL": "./Style/T3.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    12.52439728,
                    55.78602365
                ]
            }
        }
    ]
};