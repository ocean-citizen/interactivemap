var json_T33Terrestrialfacilitiesconstructionandmaintenance = {
    "type": "FeatureCollection",
    "name": "T33Terrestrialfacilitiesconstructionandmaintenance",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 1,
                "Lat": 40.34957408,
                "Lon": 18.16731277,
                "Parter": "1 Coordinator",
                "Country": "Italy",
                "PI": "Sergio Rossi",
                "Short": "UNISAL",
                "Style_URL": "./Style/T3.qml",
                "svg_URL": "./Style/T3.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    18.16731277,
                    40.34957408
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "ID": 2,
                "Lat": 32.82616636,
                "Lon": 34.95663232,
                "Parter": "2 Partner",
                "Country": "Israel",
                "PI": "Baruch Rinkevich",
                "Short": "IOLR",
                "Style_URL": "./Style/T3.qml",
                "svg_URL": "./Style/T3.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    34.95663232,
                    32.82616636
                ]
            }
        },
        {
            "id": "2",
            "type": "Feature",
            "properties": {
                "ID": 5,
                "Lat": 41.38530625,
                "Lon": 2.196064293,
                "Parter": "5 Partner",
                "Country": "Spain",
                "PI": "Enrique Isla",
                "Short": "CSIC",
                "Style_URL": "./Style/T3.qml",
                "svg_URL": "./Style/T3.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.196064293,
                    41.38530625
                ]
            }
        },
        {
            "id": "3",
            "type": "Feature",
            "properties": {
                "ID": 6,
                "Lat": 41.39471975,
                "Lon": 2.153917085,
                "Parter": "6 Partner",
                "Country": "Spain",
                "PI": "Marc Garcia-Duran Huet",
                "Short": "UGI",
                "Style_URL": "./Style/T3.qml",
                "svg_URL": "./Style/T3.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.153917085,
                    41.39471975
                ]
            }
        },
        {
            "id": "4",
            "type": "Feature",
            "properties": {
                "ID": 7,
                "Lat": 28.09909899,
                "Lon": -15.41989115,
                "Parter": "7 Partner",
                "Country": "Spain",
                "PI": "Otero Ferrer Francisco",
                "Short": "ULPGC",
                "Style_URL": "./Style/T3.qml",
                "svg_URL": "./Style/T3.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -15.41989115,
                    28.09909899
                ]
            }
        },
        {
            "id": "5",
            "type": "Feature",
            "properties": {
                "ID": 15,
                "Lat": 29.55442633,
                "Lon": 34.97036524,
                "Parter": "15 Partner",
                "Country": "Israel",
                "PI": "Julia Rozental",
                "Short": "V-CORALS",
                "Style_URL": "./Style/T3.qml",
                "svg_URL": "./Style/T3.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    34.97036524,
                    29.55442633
                ]
            }
        }
    ]
};