var json_T13Legalassessment = {
    "type": "FeatureCollection",
    "name": "T13Legalassessment",
    "crs": {
        "type": "name",
        "properties": {
            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "ID": 6,
                "Lat": 41.39471975,
                "Lon": 2.153917085,
                "Parter": "6 Partner",
                "Country": "Spain",
                "PI": "Marc Garcia-Duran Huet",
                "Short": "UGI",
                "Style_URL": "./Style/T1.qml",
                "svg_URL": "./Style/T1.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.153917085,
                    41.39471975
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "ID": 7,
                "Lat": 28.09909899,
                "Lon": -15.41989115,
                "Parter": "7 Partner",
                "Country": "Spain",
                "PI": "Otero Ferrer Francisco",
                "Short": "ULPGC",
                "Style_URL": "./Style/T1.qml",
                "svg_URL": "./Style/T1.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -15.41989115,
                    28.09909899
                ]
            }
        },
        {
            "id": "2",
            "type": "Feature",
            "properties": {
                "ID": 8,
                "Lat": 41.38680116,
                "Lon": 2.16393047,
                "Parter": "8 Partner",
                "Country": "Spain",
                "PI": "Andrea Gori",
                "Short": "UB",
                "Style_URL": "./Style/T1.qml",
                "svg_URL": "./Style/T1.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    2.16393047,
                    41.38680116
                ]
            }
        },
        {
            "id": "3",
            "type": "Feature",
            "properties": {
                "ID": 12,
                "Lat": 50.7369236,
                "Lon": -3.534859647,
                "Parter": "12 Partner",
                "Country": "United Kingdom",
                "PI": "Ruth Thurstan",
                "Short": "UNEXE",
                "Style_URL": "./Style/T1.qml",
                "svg_URL": "./Style/T1.svg"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -3.534859647,
                    50.7369236
                ]
            }
        }
    ]
};