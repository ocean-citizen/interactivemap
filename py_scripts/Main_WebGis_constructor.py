# -*- coding: utf-8 -*-
"""
Created on Tue Jul  2 15:12:36 2024

@author: misch
"""


#import pandas as pd
from data_handler_bib import (csv2gpkg, gpkg2js)
from index_html_generator import build_html

def main():
    
    # Index.html version control. Will be displayed if website get launched

    config = {
    'version' : 'v.0.2.1.0',
    
    'base_dir' :        './..',
    'fdir_Partner' :    'input_data/Partner/Partner_metadata.csv',
    'fdir_TaskGroup'  : 'input_data/TaskGroup/TaskGroups_metadata.csv',
    'fdir_Sensordata' : 'input_data/Sensordata/Sensor_metadata.CSV',
    
    'SensorAttr_alwaysVisible' : {'Operator' : True,
                                'Intervention Area' : True,
                                'Operation Period' : True,
                                'Sampling Frequnecy': False,
                                'Related Task': True,
                                'Status': True,
                                'Action': True,
                                'Sensor Type': False,
                                'Oceanic Variable': True,
                                'Short Discription': False,
                                'Person of Contact': True,
                                'Availability': True,
                                'Comments': False,
                                'Attachment_PDF': False}
    }
    
    
    #__________________________________________________________________________
    # Data import section and handel
    
    # navigates to target directory and convert csv tables into gepackages
    #csv2gpkg(config)  #data_list = csv2gpkg(config)
    
    # convert all created geopackages to javascript files
    #gpkg2js(config)

    # html Constructor
    build_html(config)
    
    #print('Test Fertig')
        
    
if __name__ == '__main__':
    main()
