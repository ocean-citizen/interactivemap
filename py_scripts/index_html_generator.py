# -*- coding: utf-8 -*-
"""
Created on Thu Aug 15 16:53:40 2024

@author: misch
"""
#import re
import os
#import shutil
#import glob
#import numpy as np
import pandas as pd
import textwrap

def build_html(config):
    
    global t0, t1, t2, t3, t4, t5, t6, t7, t8, counter
    t0 = "\n"
    t1 = "\n\t"
    t2 = "\n" + 2* "\t"
    t3 = "\n" + 3* "\t"
    t4 = "\n" + 4* "\t"
    t5 = "\n" + 5* "\t"
    t6 = "\n" + 6* "\t"
    t7 = "\n" + 7* "\t"
    t8 = "\n" + 8* "\t"
    counter = 399

    fdir_dataframe = os.path.join(config['base_dir'], 'html_struct.pkl')
    if os.path.isfile(fdir_dataframe):
        df = pd.read_pickle(fdir_dataframe)
        
    version = config['version']
    fdir_output = os.path.join(config['base_dir'], 'index.html')
    
    with  open(fdir_output, 'w', encoding="ascii") as outfile:
        
        wline = get_header()    
        outfile.write(wline)
        
        wline = get_body(df)
        outfile.write(wline) 
        
        wline = get_canvas()
        outfile.write(wline) 
        
        wline = get_title()
        outfile.write(wline) 
        
        wline = get_controler(version)
        outfile.write(wline) 
        
        wline = get_basemaps()
        outfile.write(wline) 
        
        wline =get_land_highlights(df)
        outfile.write(wline) 
        
        wline = get_partners(df)
        outfile.write(wline) 
        
        wline = get_TaskGroups(df)
        outfile.write(wline) 

        wline = get_SensorData(df, config)
        outfile.write(wline) 
        
        wline = get_LayerTree(df, config)
        outfile.write(wline)
        
        wline = get_LayerFunction(df)
        outfile.write(wline)
        
        wline = get_resetLables(df)
        outfile.write(wline)
        
        wline =get_EndOfScript()
        outfile.write(wline)
        
    print(f'>>\index.html created: {fdir_output}')

# <<<<<<<<<<<<<<<<<<<<<<<<<<< Header >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def get_header():

    html_block = textwrap.dedent("""""")
    
    html_block +=      "<!doctype html>"
    html_block += t0 + "<html lang=\"en\">"
    html_block += t1 + "<head>"
    html_block += t2 + "<meta charset=\"utf-8\">"
    html_block += t2 + "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">"
    html_block += t2 + "<meta name=\"viewport\" content=\"initial-scale=1,user-scalable=no,maximum-scale=1,width=device-width\">"
    html_block += t2 + "<meta name=\"mobile-web-app-capable\" content=\"yes\">"
    html_block += t2 + "<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">"
    html_block += t2 + "<link rel=\"stylesheet\" href=\"css/leaflet.css\">"
    html_block += t2 + "<link rel=\"stylesheet\" href=\"css/L.Control.Layers.Tree.css\">"
    html_block += t2 + "<link rel=\"stylesheet\" href=\"css/qgis2web.css\">"
    html_block += t2 + "<link rel=\"stylesheet\" href=\"css/fontawesome-all.min.css\">"
    html_block += t2 + "<link rel=\"stylesheet\" href=\"css/leaflet-measure.css\">"
    html_block += t2 + "<style>"
    html_block += t2 + "html, body, #map {"
    html_block += t3 + "width: 100%;"
    html_block += t3 + "height: 100%;"
    html_block += t3 + "padding: 0;"
    html_block += t3 + "margin: 0;"
    html_block += t2 + "}"
    html_block += t2 + "</style>"
    html_block += t2 + "<title>Ocean Citizen</title>"
    html_block += t1 + "</head>"
        
  
    return html_block

# <<<<<<<<<<<<<<<<<<<<<<<<<<<  Body  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def get_body(df):
    

    # Get the 'name' column
    #html_block = ""
    
    html_block = textwrap.dedent("""""")
    
    html_block += t1 + "<body>"
    html_block += t2 + "<div id=\"map\">"
    html_block += t2 + "</div>"
    html_block += t2 + "<script src=\"js/qgis2web_expressions.js\"></script>"
    html_block += t2 + "<script src=\"js/leaflet.js\"></script>"
    html_block += t2 + "<script src=\"js/L.Control.Layers.Tree.min.js\"></script>"
    html_block += t2 + "<script src=\"js/leaflet.rotatedMarker.js\"></script>"
    html_block += t2 + "<script src=\"js/leaflet.pattern.js\"></script>"
    html_block += t2 + "<script src=\"js/leaflet-hash.js\"></script>"
    html_block += t2 + "<script src=\"js/Autolinker.min.js\"></script>"
    html_block += t2 + "<script src=\"js/rbush.min.js\"></script>"
    html_block += t2 + "<script src=\"js/labelgun.min.js\"></script>"
    html_block += t2 + "<script src=\"js/labels.js\"></script>"
    html_block += t2 + "<script src=\"js/leaflet-measure.js\"></script>"
    
    name_column = df['name']
    for name in name_column:
        html_block += t2 + f'<script src="data/{name}.js"></script>'
    html_block +=  t2 + "<script>"
    
    html_block += t2 + "var highlightLayer;"
    html_block += t2 + "function highlightFeature(e) {"
    html_block += t3 + "highlightLayer = e.target;"
    html_block += t0
    html_block += t3 + "if (e.target.feature.geometry.type === 'LineString' || e.target.feature.geometry.type === 'MultiLineString') {"
    html_block += t4 + "highlightLayer.setStyle({"
    html_block += t5 + "color: '#37b7ca',"
    html_block += t4 + "});"
    html_block += t3 + "} else {"
    html_block += t4 + "highlightLayer.setStyle({"
    html_block += t5 + "fillColor: '#37b7ca',"
    html_block += t5 + "fillOpacity: 1"
    html_block += t4 + "});"
    html_block += t3 + "}"
    html_block += t2 + "}"

    return  html_block

# <<<<<<<<<<<<<<<<<<<<<<<<  Get Canvas  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def get_canvas():
    html_block = textwrap.dedent("""""")

    html_block += t2 + "var map = L.map('map', {"
    html_block += t3 + "zoomControl:false, maxZoom:28, minZoom:1"
    html_block += t2 + "}).fitBounds([[26.140258004168704,-39.2740104935946],[71.00542368463537,58.56148675991207]]);"
    html_block += t2 + "var hash = new L.Hash(map);"
    html_block += t2 + "map.attributionControl.setPrefix('<a href=\"https://github.com/tomchadwin/qgis2web\" target=\"_blank\">qgis2web</a> &middot; <a href=\"https://leafletjs.com\" title=\"A JS library for interactive maps\">Leaflet</a> &middot; <a href=\"https://qgis.org\">QGIS</a>');"
    html_block += t2 + "var autolinker = new Autolinker({truncate: {length: 30, location: 'smart'}});"
    html_block += t2 + "// remove popup's row if \"visible-with-data\""
    html_block += t2 + "function removeEmptyRowsFromPopupContent(content, feature) {"
    html_block += t3 + "var tempDiv = document.createElement('div');"
    html_block += t3 + "tempDiv.innerHTML = content;"
    html_block += t3 + "var rows = tempDiv.querySelectorAll('tr');"
    html_block += t3 + "for (var i = 0; i < rows.length; i++) {"
    html_block += t4 + "var td = rows[i].querySelector('td.visible-with-data');"
    html_block += t4 + "var key = td ? td.id : '';"
    html_block += t4 + "if (td && td.classList.contains('visible-with-data') && feature.properties[key] == null) {"
    html_block += t5 + "rows[i].parentNode.removeChild(rows[i]);"
    html_block += t4 + "}"
    html_block += t3 + "}"
    html_block += t3 + "return tempDiv.innerHTML;"
    html_block += t2 + "}"
    html_block += t2 + "// add class to format popup if it contains media"
    html_block += t2 + "function addClassToPopupIfMedia(content, popup) {"
    html_block += t3 + "var tempDiv = document.createElement('div');"
    html_block += t3 + "tempDiv.innerHTML = content;"
    html_block += t3 + "if (tempDiv.querySelector('td img')) {"
    html_block += t4 + "popup._contentNode.classList.add('media');"
    html_block += t5 + "// Delay to force the redraw"
    html_block += t5 + "setTimeout(function() {"
    html_block += t6 + "popup.update();"
    html_block += t5 + "}, 10);"
    html_block += t3 + "} else {"
    html_block += t4 + "popup._contentNode.classList.remove('media');"
    html_block += t3 + "}"
    html_block += t2 + "}"
    html_block += t0
                
    return  html_block

# <<<<<<<<<<<<<<<<<<<<<<<<<  Get Title  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def get_title():
     
    html_block = textwrap.dedent("""""")
  
    eu_logo = "EU_Logo.png"
    oc_logo = "OC_Logo.png"
    legend_logo ="AmpelLegend_hor.png"
    
    # Get OC logo
    html_block += t2 + "var title = new L.Control({'position':'topleft'});"
    html_block += t2 + "title.onAdd = function (map) {"
    html_block += t3 + "this._div = L.DomUtil.create('div', 'info');"
    html_block += t3 + "this.update();"
    html_block += t3 + "return this._div;"
    html_block += t2 + "};"
    html_block += t2 + "title.update = function () {"
    html_block += t3 +f"this._div.innerHTML = '<img src=\"images/{oc_logo}\" alt=\"Ocean Citizen Logo\" style=\"width:200px;height:auto;\">';"
    html_block += t2 + "};"
    html_block += t2 + "title.addTo(map);"
    html_block += t0
    
    # Get EU logo
    html_block += t2 + "var title1 = new L.Control({'position':'bottomleft'});"
    html_block += t2 + "title1.onAdd = function (map) {"
    html_block += t3 + "this._div = L.DomUtil.create('div', 'info');"
    html_block += t3 + "this.update();"
    html_block += t3 + "return this._div;"
    html_block += t2 + "};"
    html_block += t2 + "title1.update = function () {"
    html_block += t3 +f"this._div.innerHTML = '<img src=\"images/{eu_logo}\" alt=\"EU Logo\" style=\"width:100px;height:auto;\">';"
    html_block += t2 + "};"
    html_block += t2 + "title1.addTo(map);"
    html_block += t0

    # Get Legend
    html_block += t2 + "var statusLegend = new L.Control({'position':'topright'});"
    html_block += t2 + "statusLegend .onAdd = function (map) {"
    html_block += t3 + "this._div = L.DomUtil.create('div', 'info');"
    html_block += t3 + "this.update();"
    html_block += t3 + "return this._div;"
    html_block += t2 + "};"
    html_block += t2 + "statusLegend .update = function () {"
    html_block += t3 +f"this._div.innerHTML = '<img src=\"images/{legend_logo}\" alt=\"legend\" style=\"width:250px;height:auto;\">';"
    html_block += t2 + "};"
    html_block += t2 + "statusLegend .addTo(map);"
    html_block += t0

    return html_block

# <<<<<<<<<<<<<<<<<<<<<<<  Get Controler  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def get_controler(version):
    html_block = textwrap.dedent("""""")    

    mytxt = "Bug reports at: <br /> mischa.schoenke@io-warnemuende.de"

    # Text space for the version controll
    html_block += t2 + "var abstract = new L.Control({'position':'topleft'});"
    html_block += t2 + "abstract.onAdd = function (map) {"
    html_block += t3 + "this._div = L.DomUtil.create('div',"
    html_block += t3 + "'leaflet-control abstract');"
    html_block += t3 + "this._div.id = 'abstract'"
    html_block += t0
    html_block += t4 + "abstract.show();"
    html_block += t4 + "return this._div;"
    html_block += t3 + "};"
    html_block += t3 + "abstract.show = function () {"
    html_block += t4 + "this._div.classList.remove(\"abstract\");"
    html_block += t4 + "this._div.classList.add(\"abstractUncollapsed\");"
    html_block += t4 +f"this._div.innerHTML = '{version}';"
    html_block += t2 + "};"
    html_block += t2 + "abstract.addTo(map);"
    html_block += t0
    
    # Text space for the bug report or any text
    html_block += t2 + "var abstract = new L.Control({'position':'topleft'});"
    html_block += t2 + "abstract.onAdd = function (map) {"
    html_block += t3 + "this._div = L.DomUtil.create('div',"
    html_block += t3 + "'leaflet-control abstract');"
    html_block += t3 + "this._div.id = 'abstract'"
    html_block += t4 + "this._div.style.width = '300px'; // Set width in pixels"
    html_block += t4 + "this._div.style.height = '50px'; // Set height in pixels"
    html_block += t4 + "abstract.show();"
    #html_block += t0
    html_block += t4 + "return this._div;"
    html_block += t3 + "};"
    html_block += t3 + "abstract.show = function () {"
    html_block += t4 + "this._div.classList.remove(\"abstract\");"
    html_block += t4 + "this._div.classList.add(\"abstractUncollapsed\");"
    html_block += t4 +f"this._div.innerHTML = '{mytxt}';"
    html_block += t2 + "};"
    html_block += t2 + "abstract.addTo(map);"
    
    # Zoom Controller
    html_block += t2 + "var zoomControl = L.control.zoom({"
    html_block += t3 + "position: 'topleft'"
    html_block += t2 + "}).addTo(map);"
    
    # Measrurement Controller
    html_block += t2 + "var measureControl = new L.Control.Measure({"
    html_block += t3 + "position: 'topleft',"
    html_block += t3 + "primaryLengthUnit: 'meters',"
    html_block += t3 + "secondaryLengthUnit: 'kilometers',"
    html_block += t3 + "primaryAreaUnit: 'sqmeters',"
    html_block += t3 + "secondaryAreaUnit: 'hectares'"
    html_block += t2 + "});"
    html_block += t2 + "measureControl.addTo(map);"
    html_block += t2 + "document.getElementsByClassName('leaflet-control-measure-toggle')[0].innerHTML = '';"
    html_block += t2 + "document.getElementsByClassName('leaflet-control-measure-toggle')[0].className += ' fas fa-ruler';"
        
    return html_block

# <<<<<<<<<<<<<<<<<<<<<<<<  Get Basemaps  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def get_basemaps():
    html_block = textwrap.dedent("""""")
    
    html_block += t2 + "var bounds_group = new L.featureGroup([]);"
    html_block += t2 + "function setBounds() {"
    html_block += t2 + "}"
    html_block += t2 + "map.createPane('pane_GoogleSatellite');"
    html_block += t2 +f"map.getPane('pane_GoogleSatellite').style.zIndex = {idz()};"
    html_block += t2 + "var layer_GoogleSatellite = L.tileLayer('https://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {"
    html_block += t3 + "pane: 'pane_GoogleSatellite',"
    html_block += t3 + "opacity: 1.0,"
    html_block += t3 + "attribution: '<a href=\"\">@Goggle 2024</a>',"
    html_block += t3 + "minZoom: 1,"
    html_block += t3 + "maxZoom: 28,"
    html_block += t3 + "minNativeZoom: 0,"
    html_block += t3 + "maxNativeZoom: 19"
    html_block += t2 + "});"   
    html_block += t2 + "layer_GoogleSatellite;"
    html_block += t2 + "map.addLayer(layer_GoogleSatellite);"
    
    html_block += t2 + "map.createPane('pane_EsriGraylight');"
    html_block += t2 +f"map.getPane('pane_EsriGraylight').style.zIndex = {idz()};"
    html_block += t2 + "var layer_EsriGraylight = L.tileLayer('http://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {"
    html_block += t3 + "pane: 'pane_EsriGraylight',"
    html_block += t3 + "opacity: 1.0,"
    html_block += t3 + "attribution: '<a href=\"\">@ESRI Light-Gray Canvas Map</a>',"
    html_block += t3 + "minZoom: 1,"
    html_block += t3 + "maxZoom: 28,"
    html_block += t3 + "minNativeZoom: 0,"
    html_block += t3 + "maxNativeZoom: 16"
    html_block += t2 + "});"
    html_block += t2 + "layer_EsriGraylight;"
    html_block += t2 + "map.addLayer(layer_EsriGraylight);"
    
    return html_block

# <<<<<<<<<<<<<<<<<<<<<<<<  Get Partners  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def get_land_highlights(df):
    
    name = "OC_Land_Participants"
    features = ['NAME','POSTAL','OC Participant']
    
    sub_df = df.loc[df['name'] == name]
    row = sub_df.iloc[0]
    
    # OC Lands Participants
    # Pop function
    def create_lands_pop_layer(name,features):
        html_block = textwrap.dedent("""""") 
        html_block += t2 + f"function pop_{name}(feature, layer)" + " {"
        
        html_block += create_layerOn() 
        html_block += create_attr_table(features)
        html_block += create_rmEmptyLayer()
    
        html_block += t2 + "}"
        html_block += t0
        return html_block
    
    html_block = textwrap.dedent("""""") 
    html_block += create_lands_pop_layer(name,features)
    html_block += craete_style_layer(row, is_interactive = 'false')
    html_block += createPane(name,idz())
    html_block += create_layer(row)
    
    return html_block

def get_partners(df):       
    # -------------------------------------------------------------------------
    # OC Partners Participants
  
    name = "OC_Partners"
    features =['Organisation','Short','Partner','PI','Lead','Participants']
    icon_Size= '[30.4, 30.4]'
    
    sub_df = df.loc[df['name'] == name]
    row = sub_df.iloc[0]
    
    def create_pop_partner_layer(name,features):
        html_block = textwrap.dedent("""""") 
        # Create pop function
        html_block += t2 + f"function pop_{name}(feature, layer)" + " {"
       
        html_block += create_layerOn()
        html_block += create_attr_table(features, add_img= True, add_scope= True)
        html_block += create_rmEmptyLayer()
        
        html_block += t2 + "}"
        html_block += t0
        
        # Create style function
        return html_block

    html_block = textwrap.dedent("""""") 
    html_block += create_pop_partner_layer(name,features)
    html_block += craete_style_layer(row, iconSize= icon_Size, is_interactive= "true")
    html_block += createPane(name,idz())
    html_block += create_layer(row)     
            
    return html_block 
 
# <<<<<<<<<<<<<<<<<<<<<<<  Get Taskgroups  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def get_TaskGroups(df):
    
    
    filter_string = 'taskgroup'
    sub_df = df.loc[df['type'] == filter_string]
    
    # Default Features frome the header of the csv file 
    features = ['ID','Lat','Lon','Parter','Country','PI','Short','Style_URL','svg_URL']


    
    # Create pop block
    def create_task_pop_layer(name,features):
        html_block = textwrap.dedent("""""")
        
        html_block += t2 +f"function pop_{name}(feature, layer)" + " {"
        
        html_block += create_layerOn()
        html_block += create_attr_table(features)
        html_block += create_rmEmptyLayer()
        
        html_block += t2 + "}"
        html_block += t0
        
        return html_block
 
        
    html_block = textwrap.dedent("""""")
    num_rows = len(sub_df)
    for i in range(num_rows):
        row = sub_df.iloc[i]
        name = row['name'] 
        html_block += create_task_pop_layer(name,features)
        html_block += craete_style_layer(row, is_interactive= "false")
        html_block += createPane(name,idz())
        html_block += create_layer(row)
      
    return html_block

# <<<<<<<<<<<<<<<<<<<<<<<  Get SensorData  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  
def get_SensorData(df, config):
    
    html_block = textwrap.dedent("""""")

    # From Config
    features = list(config['SensorAttr_alwaysVisible'].keys())
    values = list(config['SensorAttr_alwaysVisible'].values())
    
    filter_string = 'sensordata'
    sub_df = df.loc[df['type'] == filter_string]
    
    # Create pop block
    def create_sensor_pop_layer(name,features,values):
        html_block = textwrap.dedent("""""")
        
        html_block += t2 +f"function pop_{name}(feature, layer)" + " {"
       
        html_block += create_layerOn()
        html_block += create_attr_table(features, add_scope = True, add_img= True, always_visible= values)      
        html_block += create_rmEmptyLayer()
        
        html_block += t2 + "}"
        html_block += t0
        
        return html_block

  
    num_rows = len(sub_df)
    for i in range(num_rows):
        row = sub_df.iloc[i]
        name = row['name'] 

        html_block += create_sensor_pop_layer(name,features,values)
        html_block += craete_style_layer(row) 
        html_block += createPane(name,idz())
        html_block += create_layer(row)
     
    return html_block

# <<<<<<<<<<<<<<<<<<<<<<<  Get Layer Tree  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
def get_LayerTree(df, config):
    
    def create_sensor_tree(sub_df):
        html_block = textwrap.dedent("""""")
        
        areas = sub_df['area'].unique()
        for area in areas:
            area_df= sub_df.loc[sub_df['area']==area]
            html_block += t2 + "{label: "
            html_block += f"'<b>{area} - Sensor Data</b>',"
            html_block += " selectAllCheckbox: true, collapsed: true, children: ["
            for index, row in area_df.iterrows():
                layer_name = row['name']
                legend_name = row['legend_name']
                marker = row['marker']
                #print(f"Row {index}: column1={layer_name}, column2={legend_name}")
                html_block += t3 + "{label: '<img src=\"legend/" + f"{marker}.png\"" + f" /> {legend_name}'," + f"layer: layer_{layer_name}" + "},"
            
            html_block += "]},"
            return html_block                           
                      
    def create_taskgroup_tree(sub_df):
        html_block = textwrap.dedent("""""")
        html_block += t2 + "{label: '<b>Task Groups</b>', selectAllCheckbox: true, collapsed: true, children: ["
        for index, row in sub_df.iterrows():
            layer_name = row['name']
            legend_name = row['legend_name']
            colour = row['label_color']
            #print(f"Row {index}: column1={layer_name}, column2={legend_name}")
            html_block += t3 + "{label: '<img src=\"legend/taskgroup.png\"/> <span style=\"color: " + f"#{colour}" + ";\"/span> " + f"{legend_name}" + "', " + f"layer: layer_{layer_name}" + "},"
        
        html_block += "]},"                       
        return html_block     
    
    def unckeck_group_layer(sub_df):
        html_block = textwrap.dedent("""""")
        for name in sub_df['name']:
            html_block += t3 + f"layer_{name},"
        return html_block 
            
    html_block = textwrap.dedent("""""")
    html_block += t2 + "var baseMaps = {};"
    html_block += t2 + "var overlaysTree = ["
    
    filter_string = 'sensordata'
    sub_df_sensor = df.loc[df['type'] == filter_string]
    html_block += create_sensor_tree(sub_df_sensor)
         
    filter_string = 'taskgroup'
    sub_df_task = df.loc[df['type'] == filter_string]
    html_block += create_taskgroup_tree(sub_df_task)
    
    html_block += t2 + "{label: '<b>Base Layer</b>', selectAllCheckbox: true, collapsed: true, children: ["
    html_block += t3 + "{label: \"Esri Gray (light)\", layer: layer_EsriGraylight},"
    html_block += t3 + "{label: \"Google Satellite\", layer: layer_GoogleSatellite},]},]"
    html_block += t0
    html_block += t3 + "var lay = L.control.layers.tree(null, overlaysTree,{"
    html_block += t4 + "collapsed: false,"
    html_block += t3 + "});"
    html_block += t0
    html_block += t2 + "var taskUncheckLayers = ["
    html_block += unckeck_group_layer(sub_df_task)
    html_block += unckeck_group_layer(sub_df_sensor)
    html_block += t3 + "layer_GoogleSatellite"
    html_block += t2 + "];"
    html_block += t0
    html_block += t3 + "taskUncheckLayers.forEach(function (layer) {"
    html_block += t4 + "if (map.hasLayer(layer)) {"
    html_block += t4 + "map.removeLayer(layer);"
    html_block += t4 + "}"
    html_block += t3 + "});"
    html_block += t3 + "lay.addTo(map);"
    html_block += t3 + "setBounds();"
    html_block += t0

    return html_block

# <<<<<<<<<<<<<<<<<<<<<<<  Get layer Function  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def get_LayerFunction(df):
    
    def add_sensor_block(sub_df):
        html_block = textwrap.dedent("""""")
        for name in sub_df['name']:
            html_block += t2 + "var i = 0;"
            html_block += t2 + f"layer_{name}" + ".eachLayer(function(layer) {"
            html_block += t3 + "var context = {"
            html_block += t4 + "feature: layer.feature,"
            html_block += t4 + "variables: {}"
            html_block += t3 + "};"
            html_block += t3 + "layer.bindTooltip((layer.feature.properties['Layer Name'] !== null?String('<div style=\"color: #323232; font-size: 12pt; font-weight: bold; font-style: italic; font-family: \\'Open Sans\\', sans-serif;\">' + layer.feature.properties['Layer Name']) + '</div>':''), {permanent: true, offset: [-0, -16], className:" + f" 'css_{name}'" + "});"
            html_block += t3 + "labels.push(layer);"
            html_block += t3 + "totalMarkers += 1;"
            html_block += t4 + "layer.added = true;"
            html_block += t4 + "addLabel(layer, i);"
            html_block += t4 + "i++;"
            html_block += t2 + "});"  
        return html_block 
    
    html_block = textwrap.dedent("""""")
    html_block += t2 + "var i = 0;"
    html_block += t2 + "layer_OC_Partners.eachLayer(function(layer) {"
    html_block += t3 + "var context = {"
    html_block += t4 + "feature: layer.feature,"
    html_block += t4 + "variables: {}"
    html_block += t3 + "};"
    html_block += t3 + "layer.bindTooltip((layer.feature.properties['Short'] !== null?String('<div style=\"color: #1f535c; font-size: 12pt; font-weight: bold; font-family: \\'MS Shell Dlg 2\\', sans-serif;\">' + layer.feature.properties['Short']) + '</div>':''), {permanent: true, offset: [-0, -16], className: 'css_OC_Partners'});"
    html_block += t3 + "labels.push(layer);"
    html_block += t3 + "totalMarkers += 1;"
    html_block += t4 + "layer.added = true;"
    html_block += t4 + "addLabel(layer, i);"
    html_block += t4 + "i++;"
    html_block += t2 + "});"

    filter_string = 'sensordata'
    sub_df_sensor = df.loc[df['type'] == filter_string]
    html_block += add_sensor_block(sub_df_sensor)

    return html_block

# <<<<<<<<<<<<<<<<<<<<<<<<  Get reset Lables  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def get_resetLables(df):
       
    filter_string = 'sensordata'
    sub_df_sensor = df.loc[df['type'] == filter_string]
    
    def get_labelsVector(df):
        html_block = textwrap.dedent("""""")
        html_block += "resetLabels([layer_OC_Partners"
        for name in df['name']:
            html_block += f", layer_{name}"
            
        html_block += "]);"
        return html_block
     
    html_block = textwrap.dedent("""""")
    html_block += t2 + get_labelsVector(sub_df_sensor)
           
    html_block += t2 + "map.on(\"zoomend\", function(){"
    html_block += t3 + get_labelsVector(sub_df_sensor)   
    html_block += t2 + "});"
    
    html_block += t2 + "map.on(\"layeradd\", function(){"
    html_block += t3 + get_labelsVector(sub_df_sensor)   
    html_block += t2 + "});"
    
    html_block += t2 + "map.on(\"layerremove\", function(){"
    html_block += t3 + get_labelsVector(sub_df_sensor)   
    html_block += t2 + "});"
    
    return html_block
    
# <<<<<<<<<<<<<<<<<<<<<<<<<  Get End of File  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    
def get_EndOfScript():    
    html_block = textwrap.dedent("""""")
    html_block += t2 + "</script>"
    html_block += t1 + "</body>"
    html_block += t0+ "</html>"
    return html_block
    
        
##_____________________________________________________________________________
# Subroutines

def create_layerOn():
    html_block = textwrap.dedent("""""")
    html_block += t3 + "layer.on({"
    html_block += t4 + "mouseout: function(e) {"
    html_block += t5 + "for (var i in e.target._eventParents) {"
    html_block += t6 + "if (typeof e.target._eventParents[i].resetStyle === 'function') {"
    html_block += t7 + "e.target._eventParents[i].resetStyle(e.target);"
    html_block += t6 + "}"
    html_block += t5 + "}"
    html_block += t4 + "},"
    html_block += t4 + "mouseover: highlightFeature,"
    html_block += t3 + "});"   
    return html_block

def create_rmEmptyLayer():
    html_block = textwrap.dedent("""""")
    html_block += t3 + "var content = removeEmptyRowsFromPopupContent(popupContent, feature);"
    html_block += t3 + "layer.on('popupopen', function(e) {"
    html_block += t4 + "addClassToPopupIfMedia(content, e.popup);"
    html_block += t3 + "});"
    html_block += t3 + "layer.bindPopup(content, { maxHeight: 400 });"
    return html_block

def createPane(name,ID):
    html_block = textwrap.dedent("""""")
    html_block += t2 +  f"map.createPane('pane_{name}');"
    html_block += t2 +  f"map.getPane('pane_{name}').style.zIndex = {ID};"
    html_block += t2 +  f"map.getPane('pane_{name}').style['mix-blend-mode'] = 'normal';"
    return html_block
    
def create_attr_table(features, add_scope = None, add_img= None, always_visible= None):
    
    if always_visible == True:
       values =  [True]*len(features)
    elif always_visible is not None and len(features) == len(always_visible):
       values = always_visible
    else:
       values = [True] *len(features)
    
    
    html_block = textwrap.dedent("""""")
    html_block += t3 + "var popupContent = '<table>\\"
   
    if add_img is True:
        html_block += t5 + "<tr>\\"
        html_block += t6 + "<td colspan=\"8\">' + (feature.properties['URL_Img'] !== null ? '<img src=\"images/' + String(feature.properties['URL_Img']) + '\">' : '') + '</td>\\"
        html_block += t5 + "</tr>\\"
    
    for i in range(len(features)):
        feature = features[i]
        table_row = f"(feature.properties['{feature}'] !== null ? autolinker.link(feature.properties['{feature}'].toLocaleString()) : '') + '</td>\\"
       
        if  values[i]:
            class_visible = "<td colspan=\"2\">'"   
        else:
            class_visible = f"<td class=\"visible-with-data\" id=\"{feature}\">'"
    
        if add_scope:
            scope = t6 + f"<th scope=\"row\">{feature}</th>\\"  
        else:
            scope = ""
    
        html_block += t5 + "<tr>\\"
        html_block += scope
        html_block += t6 + class_visible + " + " + table_row 
        html_block += t5 + "</tr>\\"
             
        
    html_block += t4 + "</table>';"  
   
    return html_block

def craete_style_layer(row, iconSize= None, is_interactive= None):                                             
    '''
    required: name, fill_color
    name = Layer Name
    marker_type = element from df column "Marker"
    layer_color = from unknown source
    icon_size = size of svg marker on map
    
    '''
    
    name = row['name']
    marker = row['marker']
    layer_color = row['label_color']

    # Default Settings
    svg_iconSize= '[32.3, 32.3]'
    if iconSize is not None:
        svg_iconSize= iconSize
        
    if is_interactive is None:
        is_interactive= 'true'  
        
    # ________________________________________________________    
    html_block = textwrap.dedent("""""")
    html_block += t2 +f"function style_{name}_0()" + " {"
    html_block += t3 + "return {"
    html_block += t4 +f"pane: 'pane_{name}',"

    if "Polygon" in marker:
        # Predefinition of Default Values:  
        html_block += t4 + "opacity: 1,"
        html_block += t4 + "color: 'rgba(35,35,35,1.0)'," # is global default
        html_block += t4 + "dashArray: '',"
        html_block += t4 + "lineCap: 'butt',"
        html_block += t4 + "lineJoin: 'miter',"
        html_block += t4 + "weight: 1.0,"
        html_block += t4 + "fill: true,"
        html_block += t4 + "fillOpacity: 1,"
        html_block += t4 +f"fillColor: '{layer_color}',"
        html_block += t4 +f"interactive: {is_interactive},"

    elif "MultiPoint" in marker: 

        html_block += t4 + "radius: 6.0,"
        html_block += t4 + "opacity: 1,"
        html_block += t4 + "color: 'rgba(35,35,35,1.0)',"
        html_block += t4 + "dashArray: '',"
        html_block += t4 + "lineCap: 'butt',"
        html_block += t4 + "lineJoin: 'miter',"
        html_block += t4 + "weight: 2.0,"
        html_block += t4 + "fill: true,"
        html_block += t4 + "fillOpacity: 1,"
        html_block += t4 +f"fillColor: '{layer_color}',"
        html_block += t4 +f"interactive: {is_interactive},"


    elif "Point" in marker:
         html_block += t4 + "radius: 8.0,"
         html_block += t4 + "opacity: 1,"
         html_block += t4 + "color: 'rgba(35,35,35,1.0)',"
         html_block += t4 + "dashArray: '',"
         html_block += t4 + "lineCap: 'butt',"
         html_block += t4 + "lineJoin: 'miter',"
         html_block += t4 + "weight: 2.0,"
         html_block += t4 + "fill: true,"
         html_block += t4 + "fillOpacity: 1,"
         html_block += t4 +f"fillColor: '{layer_color}',"
         html_block += t4 +f"interactive: {is_interactive},"

    
    elif "svg" in marker:
         html_block += t4 + "rotationAngle: 0.0,"
         html_block += t4 + "rotationOrigin: 'center center',"
         html_block += t4 + "icon: L.icon({"
         html_block += t5 +f"iconUrl: 'markers/{marker}',"
         html_block += t5 +f"iconSize: {svg_iconSize}"
         html_block += t4 + "}),"
         html_block += t4 +f"interactive: {is_interactive},"
        
    else:
        print("[Error]: in 'craete_style_layer', undefinded layer key agument" )    
        
    html_block += t3 + "}"  
    html_block += t2 + "}"    
    
    return html_block

def create_layer(row):                                             
    '''
    required: name, fill_color
    name = Layer Name
    marker_type = element from df column "Marker"
    layer_color = from unknown source
    icon_size = size of svg marker on map
    
    '''
    
    name = row['name']
    marker = row['marker']

  
    # ________________________________________________________    
    html_block = textwrap.dedent("""""")
    html_block += t2 +f"var layer_{name} = new L.geoJson(json_{name}, " + "{"
    html_block += t3 + "attribution: '',"
    html_block += t3 + "interactive: true,"
    html_block += t3 +f"dataVar: 'json_{name}',"
    html_block += t3 +f"layerName: 'layer_{name}',"
    html_block += t3 +f"pane: 'pane_{name}',"
    html_block += t3 +f"onEachFeature: pop_{name},"
    
    
    if "Polygon" in marker:
        html_block += t3 +f"style: style_{name}_0,"


    elif "MultiPoint" in marker or "Point" in marker: 
        html_block += t3 + "pointToLayer: function (feature, latlng) {"
        html_block += t4 + "var context = {"
        html_block += t5 + "feature: feature,"
        html_block += t5 + "variables: {}"
        html_block += t4 + "};"
        html_block += t4 +f"return L.circleMarker(latlng, style_{name}_0(feature));"
        html_block += t3 + "},"
        
    elif "svg" in marker:
        html_block += t3 + "pointToLayer: function (feature, latlng) {"
        html_block += t4 + "var context = {"
        html_block += t5 + "feature: feature,"
        html_block += t5 + "variables: {}"
        html_block += t4 + "};"
        html_block += t4 +f"return L.marker(latlng, style_{name}_0(feature));"
        html_block += t3 + "},"
        
    else:
        print("[Error]: in 'craete_layer', undefinded layer key agument" )    
        
    
    html_block += t2 + "});"
    html_block += t2 +f"bounds_group.addLayer(layer_{name});"
    html_block += t2 +f"map.addLayer(layer_{name});"   
    return html_block

def idz():
    global counter  # Access the global variable
    counter += 1    # Increment the counter by 1
    return counter  # Return the incremented value
    

