import os
import json
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point

# fdir_Project, fdir_Partner, fdir_TaskGroup, fdir_Sensordata
svg_color_map = {
    'T1': 'f85c01ff',
    'T2': 'd4c01aff',
    'T3': '21bd0aff',
    'T4': '08b5b3ff',
    'T5': '1542d3ff',
    'T6': 'aa15d3ff',
    'T7': 'd3152cff'
    }
#color_code = color_map.get(task_prefix[:2], '000000')

geometry_color_map = { 
    'OnGoing': 'rgba(253,246,33,1.0)',
    'Delayed': 'rgba(219,90,99,1.0)',
    'Completed': 'rgba(92,218,29,1.0)'
    }


# Output folder for created geopackages
global fdir_geopackage
fdir_geopackage = 'exported_geopackages/'

def partner_csv2gpkg(config):
    
    base_dir= config['base_dir']
    metadata= config['fdir_Partner'] 

    df= get_dataframe_mask()
    
    output_file_name = 'OC_Partners.gpkg'
    #output_file_name2= 'OC_Land_Participants.gpkg'
  
    
    csv_path = os.path.join(base_dir ,metadata)
    
    if not os.path.isfile(csv_path):
        print('>>\tERROR: Targer directory for "Partner" data ws not found')
        print(f'>>\tSource dir: {csv_path}')
        return
       
    geopackage_path = os.path.join(base_dir, fdir_geopackage)
    if not os.path.isdir(geopackage_path):
        print('>>\tERROR: Unable to open export folder for created geopacksges')
        print(f'>>\tTarget dir: {geopackage_path}')
        return  
    
    geopackage_file = os.path.join(geopackage_path, output_file_name)
    
    # Read the metadata CSV file
    metadata_df = pd.read_csv(csv_path , delimiter=';')
    
    # Modify the Logo_URL column
    if not os.path.isdir("../images/Partner_Logos"):
        os.mkdir("../images/Partner_Logos")
    
    metadata_df['URL_Img'] =  '/Partner_Logos/' + metadata_df['Short'] + '.png'
    
    # Create geometry from Lat and Lon columns
    metadata_df['geometry'] = metadata_df.apply(lambda row: Point(row['Lon'], row['Lat']), axis=1)
    
    # Convert to GeoDataFrame
    gdf = gpd.GeoDataFrame(metadata_df, geometry='geometry')
    
    # Define the projection (assuming WGS84)
    gdf.set_crs(epsg=4326, inplace=True)
    
    # Save to GeoPackage
    gdf.to_file(geopackage_file, layer='OC_Partners',OVERWRITE='YES', driver='GPKG')

    # add infos to dataframe
    df.loc[len(df)] = ['OC_Partners', 'OC Partners', None, 'partner', '000000','OC_Partners.png','OC_Partners.svg']
    print('[csv_to_gpkg]: OC_Partners.gpkg created')
    
    df.loc[len(df)] = ['OC_Land_Participants','OC Participants', '', 'partner', 'rgba(143,125,4,0.30196078431372547)','OC_Land_Participants.png','Polygon']
    print('[csv_to_gpkg]: OC__Land_Participants added to list')
    
    return df

def taskGroup_csv2gpkg(config):

    base_dir= config['base_dir']
    metadata_csv= config['fdir_TaskGroup']     

    # Dynamic Output_file_name
    df= get_dataframe_mask()
    
    csv_path = os.path.join(base_dir,metadata_csv)
    
    if not os.path.isfile(csv_path):
        print('>>\tERROR: Targer directory for "TaskGroup" data ws not found')
        print(f'>>\tSource dir: {csv_path}')
        return
    
    geopackage_path = os.path.join(base_dir, fdir_geopackage)
    if not os.path.isdir(geopackage_path):
        print('>>\tERROR: Unable to open export folder for created geopacksges')
        print(f'>>\tTarget dir: {geopackage_path}')
        return  
    
    # Define the input file path and read the tab-separated table into a DataFrame
    # Read the metadata CSV file
    
    # Define file paths
    metadata_df = pd.read_csv(csv_path, delimiter=';')

   # Defin e the columns to keep in each GeoPackage file
    columns_to_keep = ['ID', 'Lat', 'Lon', 'Parter', 'Country', 'PI', 'Short']
    
    # Define the task columns from T1.1 to T7.4
    task_columns = metadata_df.columns[7:]
    
    # Iterate over each task column and create a GeoPackage file
    for task in task_columns:
        # Filter rows where the task value is not zero
        task_df = metadata_df[metadata_df[task] != 0]
    
        # Create a GeoDataFrame
        gdf = gpd.GeoDataFrame(
            task_df[columns_to_keep],
            geometry=[Point(xy) for xy in zip(task_df['Lon'], task_df['Lat'])],
            crs='EPSG:4326'
        )
    
        # Define the output file name
        output_file =  os.path.join(geopackage_path, f'{task}.gpkg')
    
        styleID = task[:2]
        gdf['Style_URL'] = './Style/' + styleID + '.qml'
        gdf['svg_URL']   = './Style/' + styleID + '.svg'
    
        # Save the GeoDataFrame to a GeoPackage
        gdf.to_file(output_file, layer=task, driver='GPKG')
        
        # add core infos to dataframe
        color_code = svg_color_map.get(styleID, '000000')
        js_name = get_valid_js_fname(task)
        df.loc[len(df)]=[ js_name, task , None , 'taskgroup', color_code,'taskgroup.png',styleID+'.svg']

        print(f"[csv_to_gpkg]: {task}.gpkg created")
        
    return df
              
def sensordata_csv2gpkg(config):

    base_dir= config['base_dir']
    fdir_sensordata_csv= config['fdir_Sensordata'] 
    
    # Dynamic Output_file_name
    df1= get_dataframe_mask()
    
    # Load the CSV file into a DataFrame
    #base_dir = os.getcwd()
    csv_path = os.path.join(base_dir, fdir_sensordata_csv)
    fdir_Sensor_geometry = os.path.join(fdir_sensordata_csv,'../Geometry_Files')
    
    if not os.path.isfile(csv_path):
        print('>>\tERROR: Targer directory for "Sensor" data ws not found')
        print(f'>>\tSource dir: {csv_path}')
        return
    
    geometry_files_dir = os.path.join(base_dir, fdir_Sensor_geometry)
    if not os.path.isdir(geometry_files_dir):
        print('>>\tERROR: Unable to open folder containing geometrie files of Sensor data')
        print(f'>>\tTarget dir: {geometry_files_dir}')
        return 
    
    geopackage_path = os.path.join(base_dir, fdir_geopackage)
    if not os.path.isdir(geopackage_path):
        print('>>\tERROR: Unable to open export folder for created geopacksges')
        print(f'>>\tTarget dir: {geopackage_path}')
        return 
    

    df = pd.read_csv(csv_path, delimiter=';')
    
    # Add new fields to the DataFrame
    df.insert(1, 'URL_Img', '')
    df.insert(len(df.columns), 'URL_Style', '')
    
    # Define base URL for PDF links
    #pdf_base_url = '<a href="dummy.pdf" target="_blank">dummy.pdf</a>'
    pdf_base_url = '<a href="pdf_attachments/dummy.pdf" target="_blank">dummy.pdf</a>'
    
    # Process each row in the DataFrame
    for index in range(len(df)):
        row = df.loc[[index]]  # selects current row from table
        geometry_type = row.iloc[0]['Geometry_Type'] # gets current'Geometry_Type
        geometry_data = row.iloc[0]['Geometry_Data'] # gets current Geometry_Data
        status = row.iloc[0]['Status']               # gets current status (e.g. done, ongoing, delayed)
        layer_name = row.iloc[0]['Layer Name']       # gets current Layer name
        area = row.iloc[0]['Intervention Area']       # gets current area
        # Handle the PDF attachment
        # checkts if the current row contains a name of an pdf document, if yes
        # a dummy html hyper reference expression is created and the name of 
        # the target pdf is inserted into the expression
        if pd.notna(row.iloc[0]['Attachment_PDF']):
            temp_pdf_url = pdf_base_url.replace('dummy.pdf', row.iloc[0]['Attachment_PDF'])
            row['Attachment_PDF'] = temp_pdf_url
        
        # Handle the URL_Style field
        style_filename = f"{geometry_type}_{status}.qml"
        row['URL_Style'] = style_filename
        
        # Handle the Imgare Attachment field chekcs if there is an attachment
        # entry if not sets a default img
        if pd.notna(row.iloc[0]['Attachment_Imgs']):
            row['URL_Img'] =  row.iloc[0]['Attachment_Imgs']
        else:
            row['URL_Img'] = 'default_Img.png'
    
        row = row.drop(columns='Geometry_Type')
        row = row.drop(columns='Geometry_Data')
    
        # Process geometry data and create GeoPackage
        gdf = None
        if geometry_type == 'Point':
            # Parse the [Lat, Lon] format
            lat, lon = map(float, geometry_data.strip('[]').split(','))
            geometry = Point(lon, lat)
            gdf = gpd.GeoDataFrame(row, geometry= [geometry], crs='EPSG:4326')
            gdf = gdf[['geometry']]  
            
            if pd.notna(row.iloc[0]['Attachment_Imgs']):
                folder_name = get_valid_js_fname(layer_name)
                row['URL_Img'] = folder_name + "/" + row['URL_Img']
                if not os.path.isdir(f"../images/{folder_name}"):
                    os.mkdir(f"../images/{folder_name}")
                    
        else:
            # Load geometry from GeoPackage
            gdf = gpd.read_file(os.path.join(geometry_files_dir, geometry_data))

           
        if (geometry_type=='MultiPoint') & pd.notna(row.iloc[0]['Attachment_Imgs']):
        # In case of a multiPoint Feature the image must be individually filled in each column
        # The filename of the images must be stored in a column called
        # fieldnames and matches to the layer geometrie information     
            
            
            # Append the first row to row_extended gdf_size times
            row_extended = []
            for _ in range(len(gdf)):
                row_extended.append(row.copy())
                
            # Convert list of DataFrames to a single concatenated DataFrame
            row = pd.concat(row_extended, ignore_index=True)
            
            # Loops over the URL_Img columns and merges the filenames to the
            # filepath
            folder_name = get_valid_js_fname(layer_name)
            
            if not os.path.isdir(f"../images/{folder_name}"):
                os.mkdir(f"../images/{folder_name}")
            
            for idx in range(len(gdf)):
                row.loc[idx, 'URL_Img'] = folder_name + "/" + gdf.loc[idx, 'filename']
                                      
            gdf = gdf[['geometry']]  
            row = row.drop(columns='Attachment_Imgs') 
            
            # Add attributes to the GeoDataFrame
            for col in row:
                #print(col)
                if col not in gdf.columns:
                    #print(f'{col}')
                    gdf[col] = row[col]
        
        else:
            # Add attributes to the GeoDataFrame
            gdf = gdf[['geometry']] 
            row = row.drop(columns='Attachment_Imgs') 
             
            for col in row:
                #print(col)
                if col not in gdf.columns:
                    #print(f'{col}')
                    gdf[col] =  row.iloc[0][col]

        # Save the GeoDataFrame as a GeoPackage
        output_filename = os.path.join(geopackage_path, f"{layer_name}.gpkg")
        
        js_name = get_valid_js_fname(layer_name)
        
        marker= geometry_type + '_' + status
       
        # adds color code to layer
        color_code = geometry_color_map.get(status, '000000')
       
        # add core infos to dataframe
        df1.loc[len(df1)] = [ js_name, layer_name, area, 'sensordata', color_code,'' ,marker]
        
        gdf.to_file(output_filename, layer=layer_name, driver="GPKG")
        print(f"[csv_to_gpkg]: {layer_name}.gpkg created")
        
    return df1    
        
def gpkg2js(config):
    
    #fdir_geopackage = 'exported_geopackages/'
    base_dir= config['base_dir']
    # location containing the created geopacktes
    input_folder = os.path.join(base_dir, fdir_geopackage)
    
    # output folder, name is predifned, otherwise can not be recognized by
    # webgis project
    output_folder = os.path.join(base_dir,'data/')
    
    # Create the output folder if it doesn't exist
    os.makedirs(output_folder, exist_ok=True)
    
    # Define the source and target CRS
    # source_crs = 'EPSG:4326'  # WGS 84 should be the input data format    <- remove in final version
    # target_crs = 'EPSG:3857'  # Web Mercator                              <- remove in final version

    target_crs = 'EPSG:4326'  # Web Mercator
    
    # Loop over each file in the input folder
    for filename in os.listdir(input_folder):
        if filename.endswith(".gpkg"):
            # Construct full file path
            gpkg_path = os.path.join(input_folder, filename)
            
            #
            print(f"\n[gpkg_to_js]:\tImport: {filename} ")
            
            # Load the GeoPackage file
            gdf = gpd.read_file(gpkg_path)
            
            source_crs = gdf.crs
            # Check the current CRS (Coordinate Reference System)
            print("[gpkg_to_js]:\tOriginal CRS:", source_crs)
            
            if (target_crs != source_crs): 
                print("[gpkg_to_js]:\tapply CRS transformation to target CRS")
                
                # Reproject the GeoDataFrame
                gdf = gdf.to_crs(target_crs)

                # Check the transformed CRS
                print("[gpkg_to_js]:\tTransformed CRS:", gdf.crs)
            
            # get valid filename, without special signes
            geojson_dict = json.loads(gdf.to_json())
            valid_filename = get_valid_js_fname(filename.split(".gpkg")[0])
            
            # Convert GeoDataFrame to GeoJSON format
            # Construct the GeoJSON dictionary with the correct order
            ordered_geojson_dict = {
                "type": geojson_dict["type"],
                "name": valid_filename,  # Using filename without extension as name
                "crs": {
                    "type": "name",
                    "properties": {
                        "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
                    }
                },
                "features": geojson_dict["features"]
            }
            
            # Prepare the final JavaScript content
            js_variable_name = "json_" +  valid_filename
            js_content = f"var {js_variable_name} = {json.dumps(ordered_geojson_dict, indent=4)};"
            
            # Construct the output file path
            output_filename = f"{valid_filename}.js"
           
            output_filename = os.path.join(output_folder, output_filename)
            
            # Write the content to a JavaScript file
            with open(output_filename, 'w') as js_file:
                js_file.write(js_content)
            
            print(f"[gpkg_to_js]:\tConverted to {output_filename}")

def csv2gpkg(config):
    

    # location containing the created geopacktes
    output_folder = os.path.join(config['base_dir'], fdir_geopackage)
    
    # Create the output folder if it doesn't exist
    os.makedirs(output_folder, exist_ok=True)
    
    # Convert partner data from csv to geopackge
    df_partner = partner_csv2gpkg(config)
 
    # Convert TaskGroup Data from csv to geopackge
    df_taskgroup = taskGroup_csv2gpkg(config)
    
    # Convert Sensor data from csv to geopackge
    df_sensordata = sensordata_csv2gpkg(config)

    # Merge all DataFrames
    data_list = pd.concat([df_partner, df_taskgroup, df_sensordata], ignore_index=True)
    fdir_dataframe = os.path.join(config['base_dir'], 'html_struct.pkl')
    data_list.to_pickle(fdir_dataframe)
    
    return data_list


## ----------------------------------------------------------------------------
# Subroutines
def get_valid_js_fname(name):
    name_out = name.replace(" ", "").replace("-", "").replace(".", "").replace(")", "").replace("(", "").replace(",","")
    return name_out

def get_dataframe_mask():
    df=pd.DataFrame([]);
    df['name'] = []
    df['legend_name'] = []
    df['area'] = []
    df['type'] = []
    df['label_color'] = [] 
    df['legend'] = []
    df['marker'] = []
    return df